﻿package Utils 
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import gs.*;
	import gs.easing.*;
	import Utils.Global;
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class OverState 
	{
		private static var global:Global = Global.getInstance();
		public function OverState() 
		{
			
		}
		public static function addInstance(instance:DisplayObject, type:uint = 1):void {
			if (type == 1 || type == 2) {
				switch (type) {
					case 1:
						instance.addEventListener(MouseEvent.ROLL_OVER, onMouseOverType1);
					break;
					case 2:
						instance.addEventListener(MouseEvent.ROLL_OVER, onMouseOverType2);
					break;
				}
				instance.addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
			}else {
				throw new Error("Provided over state type is not supported");
			}
		}
		private static function onMouseOverType1(event:MouseEvent):void {
			var target:DisplayObject = event.currentTarget as DisplayObject;
			TweenMax.killTweensOf(target, true);
			TweenMax.to(target, global.transitionDuration, {glowFilter:global.overStateGlowType1 ,ease:Expo.easeOut});
		}
		private static function onMouseOverType2(event:MouseEvent):void {
			var target:DisplayObject = event.currentTarget as DisplayObject;
			TweenMax.killTweensOf(target, true);
			TweenMax.to(target, global.transitionDuration, {glowFilter:global.overStateGlowType2 ,ease:Expo.easeOut});
		}
		private static function onMouseOut(event:MouseEvent):void {
			var target:DisplayObject = event.currentTarget as DisplayObject;
			TweenMax.killTweensOf(event.target, true);
			TweenMax.to(target, global.transitionDuration, {glowFilter:{ alpha:0 , blurX:0, blurY:0, strength:0, quality:BitmapFilterQuality.LOW, inner:false, knockout:false} ,ease:Expo.easeOut});
		}
	}
	
}