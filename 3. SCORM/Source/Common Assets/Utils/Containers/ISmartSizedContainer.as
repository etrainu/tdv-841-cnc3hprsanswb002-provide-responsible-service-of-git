package Utils.Containers 
{
	
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public interface ISmartSizedContainer 
	{
		function rearrange():void;
		function get autoStretch():Boolean;
		function set autoStretch(b:Boolean):void;
	}
	
}