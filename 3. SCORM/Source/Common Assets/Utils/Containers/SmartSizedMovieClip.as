package Utils.Containers 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class SmartSizedMovieClip extends MovieClip implements ISmartSizedContainer
	{
		private var _autoStretch:Boolean = false;
		
		public function SmartSizedMovieClip() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, initStageEvents, false, 0, true);
			this.addEventListener(Event.REMOVED_FROM_STAGE, terminateStageEvents, false, 0, true);
		}
		protected function initStageEvents(event:Event = null):void {
			//Defult
			//this.removeEventListener(Event.ADDED_TO_STAGE, initStageEvents);
			stage.addEventListener(Event.RESIZE, stageResizeHandler, false, 0, true);
			stageResizeHandler();
			
			//Added Codes Here
		}
		
		
		protected function terminateStageEvents(event:Event = null):void {
			//Defult
			stage.removeEventListener(Event.RESIZE, stageResizeHandler);
			//Added Codes Here
		}
		
		//Public Methods
		public function rearrange():void {
			if (this.stage) {
				stageResizeHandler();
			}
		}
		
		
		//Getters and setters
		public function get autoStretch():Boolean {
			return _autoStretch
		}
		public function set autoStretch(b:Boolean):void {
			_autoStretch = b;
		}
		
		
		//Event Handlers
		protected function stageResizeHandler(event:Event = null):void {
			if (_autoStretch) {
				this.width = stage.stageWidth;
				this.height = stage.stageHeight;
			}
			trace("Resized");
		}
		
	}

}