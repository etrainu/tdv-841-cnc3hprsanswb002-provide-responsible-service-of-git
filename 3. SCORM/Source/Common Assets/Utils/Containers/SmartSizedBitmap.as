package Utils.Containers 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class SmartSizedBitmap extends Bitmap  implements ISmartSizedContainer
	{
		private var _autoStretch:Boolean = false;
		private var initialized:Boolean = false;
		
		public var autoDistruct:Boolean;
		public function SmartSizedBitmap() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, initStageEvents, false, 0, true);
			this.addEventListener(Event.REMOVED_FROM_STAGE, terminateStageEvents, false, 0, true);
			autoDistruct = false;
		}
		protected function initStageEvents(event:Event = null):void {
			if (!initialized) {
				initialized = true;
				
				//Defult
				//this.removeEventListener(Event.ADDED_TO_STAGE, initStageEvents);
				stage.addEventListener(Event.RESIZE, stageResizeHandler, false, 0, true);
				stageResizeHandler();
				
				//Added Codes Here
			}
		}
		protected function terminateStageEvents(event:Event = null):void {
			//Defult
			trace("removing a SmartSizedBitmap!!!!!!!!!!!!!!!!!!!!!");
			stage.removeEventListener(Event.RESIZE, stageResizeHandler);
			if(autoDistruct){
				this.bitmapData = null;
			}
			//Added Codes Here
		}
		
		//Public Methods
		public function rearrange():void {
			if (this.stage) {
				stageResizeHandler();
			}
		}
		//Getters and setters
		public function get autoStretch():Boolean {
			return _autoStretch
		}
		public function set autoStretch(b:Boolean):void {
			_autoStretch = b;
		}
		
		
		//Event Handlers
		protected function stageResizeHandler(event:Event = null):void {
			if (_autoStretch) {
				this.width = stage.stageWidth;
				this.height = stage.stageHeight;
			}
			trace("Smart Bitmap Resized");
		}
		
	}

}