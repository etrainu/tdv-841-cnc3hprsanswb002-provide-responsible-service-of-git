package Utils 
{
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author ...
	 */
	public class TooltipManager 
	{
		private static var tipPool:Array = new Array();
		private static var global:Global;
		public function TooltipManager() 
		{
		}
		public static function setTip(target:InteractiveObject , faMessage:String, enMessage:String, duration:Number = -1):void {
			target.addEventListener(MouseEvent.MOUSE_OVER, objectOver, false, 0, true);
			target.addEventListener(MouseEvent.MOUSE_OUT, objectOut, false, 0, true);
			var recordFound:int = -1;
			for (var i:int = 0; i < tipPool.length; i++ ) {
				if (tipPool[i].target == target) {
					recordFound = i;
					break
				}
			}
			if (recordFound == -1) {
				tipPool.push({target:target, faMessage:faMessage, enMessage:enMessage, duration:duration});
			}else {
				tipPool[recordFound] = { target:target, faMessage:faMessage, enMessage:enMessage, duration:duration };
			}
			
		}
		private static function objectOver(event:MouseEvent):void {
			var target:InteractiveObject = event.currentTarget as InteractiveObject;
			global = Global.getInstance();
			var cellData:Object;
			for (var i:int = 0; i < tipPool.length; i++ ) {
				if (tipPool[i].target == target) {
					cellData = tipPool[i];
					break
				}
			}
			if (cellData) {
				if (cellData.duration > 0) {
					DoteesHint.attach(cellData[global.courseLanguage+"Message"], cellData.duration);	
				}else {
					DoteesHint.attach(cellData[global.courseLanguage+"Message"]);
				}
				
			}
			
		}
		private static function objectOut(event:MouseEvent):void {
			DoteesHint.remove();
		}
	}

}