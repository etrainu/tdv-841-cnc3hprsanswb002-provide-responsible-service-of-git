package Utils.calculations 
{
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class ExtendedRnd 
	{
		
		public function ExtendedRnd() 
		{
			
		}
		private static function getBowndedRand(lowerLimit:Number, upperLimit:Number):uint{
			var myRnd:uint = Math.round(Math.random()*(upperLimit-lowerLimit))+(lowerLimit);
			return myRnd
		}
		public static function getUniqueRand(length:uint, lowerLimit:Number, upperLimit:Number):Vector.<uint>{
			var generatedRndVector:Vector.<uint> = new Vector.<uint>();
			var generatedNum:uint;
			var isacceptable:Boolean;
			for(var i:int =0; i< length; i++){
				isacceptable = true;
				generatedNum = getBowndedRand(lowerLimit,upperLimit);
				for(var j:int =0; j< generatedRndVector.length; j++){
					if(generatedNum == generatedRndVector[j]){
						isacceptable = false;
						break
					}
				}
				if(isacceptable){
					generatedRndVector.push(generatedNum);
				}else{
					i--
				}
			}
			return generatedRndVector
		}
		
	}

}