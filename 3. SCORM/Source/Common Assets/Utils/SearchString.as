package Utils 
{
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	import com.adobe.utils.*;
	public class SearchString 
	{
		/*public static var startIndex:uint = 0;
		public static var endIndex:uint = 0;
		public static var indexPos:Vector.<int>;
		public static var indexPosList:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
		public static var trimmedResult:String;*/
		public function SearchString() 
		{
			
		}
		public static function getIndexes(text:String, query:String, exactMatch:Boolean = false, caseSensative:Boolean = false):Vector.<Vector.<int>> {
			var queryArr:Array = query.split(" ");
			var regString:String = new String();
			for (var i:int = 0; i<queryArr.length; i++) {
				if (!exactMatch) {
					//regString+=".*?";
					regString+=".?";
				}else{
					regString+=" ";
				}
				regString+=queryArr[i];
			}
			var flag:String;
			if (!caseSensative) {
				flag = "igm";
			}else{
				flag = "gm";
			}
			
			var searchPattern:RegExp = new RegExp(regString, flag);
			trace(text);
			var matchingArray:Array = text.match(searchPattern);
			//trace("---------------------------RESULTS---------------------\n"+matchingArray);
			var startIndex:uint = 0;
			var endIndex:uint = 0;
			var indexPos:Vector.<int>;
			var indexPosList:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
			var trimmedResult:String;
			for (i = 0; i < matchingArray.length; i++) {
				indexPos = new Vector.<int>();
				trimmedResult = StringUtil.trim(matchingArray[i]);
				startIndex = text.indexOf(trimmedResult, endIndex-1);
				endIndex = startIndex + trimmedResult.length;
				indexPos.push(startIndex);
				indexPos.push(endIndex);
				trace("indexPos: " + i + "\n" + indexPos + "\n");
				trace(text.slice(startIndex, endIndex) );
				indexPosList.push(indexPos);
				
			}
			return indexPosList
		}
		
	}

}