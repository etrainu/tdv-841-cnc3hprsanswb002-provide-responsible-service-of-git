﻿package Utils 
{
	
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class FixPersian 
	{
		
		public function FixPersian() 
		{
			
		}
		public static function replaceYa(inputStr:String):String {
			var myPattern:RegExp = /ی/g;  
			var replacedStr: String = inputStr.replace(myPattern, "ي");
			return replacedStr;
		}
		
	}
	
}