﻿package Utils {
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import hintBG;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	import fl.text.TLFTextField;
	import flash.text.TextFieldAutoSize;
	
	public class TootipView extends MovieClip {
		public var backGround:hintBG;
		public var hintText:TextField;
		
		// Constants:
		public var maxWidth:uint = 300;
		private const textMargin:int = 5;
		private const textSize:int = 11;
		private const textFont:String = "Calibri";
		private const textColor:String = "0x333333";
		private const cursorHeight:int = 15;
		private const cursorWidth:int = 15;
		// Public Properties:
		// Private Properties:	
		private var _text:String = "";
		
		private static var instance:DoteesHint = null;
		private static var allowInstantiation:Boolean = false;
		
		public function TootipView() {
			// constructor code
		}
		
		// Public Methods:
		public function set text(t:String):void {
			_text = t;
			hintText.wordWrap = false;
			hintText.text = _text;
			hintText.autoSize = TextFieldAutoSize.LEFT;
			if (hintText.width > maxWidth) {
				hintText.wordWrap = true;
				hintText.width = maxWidth;
			}
			backGround.width = hintText.width+(2 * textMargin);
			backGround.height = hintText.height+(textMargin);
		}
		public function get text():String {
			return _text;
		}
	}
	
}
