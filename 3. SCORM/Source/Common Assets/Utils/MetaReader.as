﻿package Utils 
{
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	import flash.events.*;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import Utils.*;
	public class MetaReader extends EventDispatcher
	{
		private var sourceXML:XML;
		private var dataXml:XML;
		public var metaRefrenceList:Array;
		private var metaLoader:URLLoader;
		private var metaReq:URLRequest;
		private var loadingItemNom:uint;
		private var metaFileExtension:String;
		private var SCOFolder:String;
		private var titleField:String;
		public function MetaReader(sourceXML:XML, metaFileExtension:String, SCOFolder:String, titleField:String){
			this.sourceXML = sourceXML;
			this.metaFileExtension = metaFileExtension;
			this.SCOFolder = SCOFolder;
			this.titleField = titleField;
		}
		public function init():void {
			dataXml = proccessXML(sourceXML);
			//trace("meta dataXml: "+dataXml);
			metaLoader = new URLLoader();
			metaLoader.addEventListener(Event.COMPLETE, loadComplete);
			metaLoader.addEventListener(IOErrorEvent.IO_ERROR, loadError);
			metaRefrenceList = new Array();
			loadingItemNom = 0;
			for (var i:uint = 0; i < dataXml.SCOList.Chapter.length(); i++) {
				for (var j:uint = 0; j < dataXml.SCOList.Chapter[i].SCO.length(); j++) {
					var scoFileName:String = dataXml.SCOList.Chapter[i].SCO[j].@file;
					var metaFileName:String = scoFileName.substr(0,scoFileName.length-3)+metaFileExtension;
					metaRefrenceList.push({chapter:i, sco:j, id:dataXml.SCOList.Chapter[i].SCO[j].@id, file:dataXml.SCOList.Chapter[i].SCO[j].@file, xmlMeta:dataXml.SCOList.Chapter[i].SCO[j].@metaCSV, title:FixPersian.replaceYa(dataXml.SCOList.Chapter[i].SCO[j].@[titleField]), description:dataXml.SCOList.Chapter[i].SCO[j].@SCODescription, externalMetaPath:SCOFolder+"/"+dataXml.SCOList.Chapter[i].SCO[j].@folderPath+"/"+metaFileName, externalMeta:""});
					trace("externalMetaPath "+SCOFolder+"/"+dataXml.SCOList.Chapter[i].SCO[j].@folderPath+"/"+metaFileName);
					trace("id on meta: "+dataXml.SCOList.Chapter[i].SCO[j].@id);
					trace("description "+dataXml.SCOList.Chapter[i].SCO[j].@SCODescription);
				}
			}
			loadExtMeta();
		}
		private function proccessXML(inputXML:XML):XML{
			var chapterString:String="<ChapterList>";
			chapterString+=inputXML..Chapter;
			chapterString+="</ChapterList>";
			var chapterXML:XML=XML(chapterString);

			var SCOString:String;
			var SCOXML:XML;
			var totalXmlString:String = "<Course><SCOList>";
			for (var i:uint = 0; i<chapterXML.Chapter.length(); i++) {
				SCOString="<Chapter>";
				SCOString+=chapterXML.Chapter[i]..SCO;
				SCOString+="</Chapter>";
				SCOXML=XML(SCOString);
				
				for (var j:uint = 0; j<SCOXML.SCO.length(); j++) {
					SCOXML.SCO[j].@folderPath = chapterXML.Chapter[i].@folderPath;
				}
				totalXmlString += SCOXML.toString();
			}
			totalXmlString += "</SCOList></Course>";
			var totalXML:XML = XML(totalXmlString);
			return totalXML
		}
		private function loadExtMeta() {
			if(loadingItemNom < metaRefrenceList.length){
				//trace("loading " + metaRefrenceList[loadingItemNom].externalMetaPath);
				metaReq = new URLRequest(metaRefrenceList[loadingItemNom].externalMetaPath);
				metaLoader.load(metaReq);
			}
		}
		private function loadComplete(event:Event):void {
			//trace("loaded data \r" + event.target.data);
			metaRefrenceList[loadingItemNom].externalMeta = FixPersian.replaceYa(event.target.data);
			//_model.metaRefrenceList = metaRefrenceList;
			this.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, loadingItemNom, metaRefrenceList.length));
			loadingItemNom += 1;
			loadExtMeta();
		}
		private function loadError(event:IOErrorEvent):void {
			loadingItemNom += 1;
			loadExtMeta();
			trace(event.text);
		}
	}
}