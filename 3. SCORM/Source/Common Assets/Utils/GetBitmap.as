﻿package Utils 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import Utils.Containers.SmartSizedBitmap;
	/**
	 * ...
	 * @author ...
	 */
	public class GetBitmap 
	{
		public static var imageData:BitmapData;
		public static var image:SmartSizedBitmap;
		public function GetBitmap() 
		{
			
		}
		public static function draw(display:DisplayObject, w:int = -1, h:int = -1):SmartSizedBitmap {
			if (w == -1) {
				w = display.width
			}
			if (h == -1) {
				h = display.height
			}
			imageData = new BitmapData(w, h, true, 0x00000000);
			imageData.draw(display);
			image = new SmartSizedBitmap();
			image.bitmapData = imageData;
			image.smoothing = true;
			image.x = display.x;
			image.y = display.y;
			image.scaleX = display.scaleX;
			image.scaleY = display.scaleY;
			return image
		}
		public static function drawRectClip(display:DisplayObject, clippingRect:Rectangle):SmartSizedBitmap {
			var w:int = clippingRect.width
			var h:int = clippingRect.height
			imageData = new BitmapData(w, h, true, 0x00000000);
			imageData.draw(display, null, null, null, clippingRect, true);
			image = new SmartSizedBitmap();
			image.bitmapData = imageData;
			image.smoothing = true;
			image.x = display.x;
			image.y = display.y;
			image.scaleX = display.scaleX;
			image.scaleY = display.scaleY;
			return image
		}
		public static function drawScaled(display:DisplayObject, scaleX:Number = 1, scaleY:Number = 1, w:int = -1, h:int = -1):SmartSizedBitmap {
			if (w == -1) {
				w = display.width * scaleX;
			}
			if (h == -1) {
				h = display.height * scaleY;
			}
			var matrix:Matrix = new Matrix();
			matrix.scale(scaleX, scaleY);
			imageData = new BitmapData(w, h, true, 0x00000000);
			imageData.draw(display, matrix);
			image = new SmartSizedBitmap();
			image.bitmapData = imageData;
			image.smoothing = true;
			image.x = display.x;
			image.y = display.y;
			image.scaleX = display.scaleX;
			image.scaleY = display.scaleY;
			return image
		}
	}

}