package Utils 
{
	/**
	 * ...
	 * @author ...
	 */
	public class TimeFormat 
	{
		
		public function TimeFormat() 
		{
			
		}
		public static function frameToTime(frame:uint, frameRate:uint):String {
			//trace("frameRate: "+frameRate);
			var outputStr:String;
			var seconds:uint = frame / frameRate;
			var minuts:uint = Math.floor(seconds / 60);
			var remSeconds:uint = seconds - (minuts * 60);
			outputStr = minuts + ":" + remSeconds;
			return outputStr
		}
	}

}