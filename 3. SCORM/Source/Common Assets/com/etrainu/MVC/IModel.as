package com.etrainu.MVC
{
	
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	import flash.net.URLRequest;
	public interface IModel 
	{
		function load(req:URLRequest):void;
		function saveSession(addProgress:Boolean = true):void;
	}
	
}