package com.etrainu.comps {
	
	import flash.display.MovieClip;
	import fl.controls.Button;
	import flash.text.TextField;
	
	public class YoutubeVideoPlayerUI extends MovieClip {
		public var backwardBtn:Button;
		public var forwardBtn:Button;
		public var pauseBtn:Button;
		public var playBtn:Button;
		public var totalTimeTxt:TextField;
		
		public function YoutubeVideoPlayerUI() {
			// constructor code
		}
	}
	
}
