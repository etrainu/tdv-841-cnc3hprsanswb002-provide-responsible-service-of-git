﻿package com.etrainu.comps
{

	import flash.display.Loader;
	import fl.controls.Button;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.system.Security;
	import flash.text.TextField;
	public class YoutubeVideoPlayer extends Sprite
	{
		private const API_PLAYER_URL:String = "http://www.youtube.com/apiplayer?version=3&enablejsapi=1";
		private static var instance:YoutubeVideoPlayer = null;
		private static var allowInstantiation:Boolean = false;


		public var backwardBtn:Button;
		public var forwardBtn:Button;
		public var pauseBtn:Button;
		public var playBtn:Button;
		public var totalTimeTxt:TextField;
		
		private var youtubePlayer:Object;
		private var youtubeLoader:Loader;
		private var initialized:Boolean;
		
		private var _width:Number = 570;
		private var _height:Number = 385;
		
		private var _ratio:Number = AspectRatio.WIDE;
		private var _source:String;
		private var ui:YoutubeVideoPlayerUI;
		
		private var stopRequest:Boolean;
		public function YoutubeVideoPlayer()
		{
				if (! allowInstantiation)
				{
					throw new Error("Error: Instantiation failed: Use YoutubeVideoPlayer.getInstance() instead of new YoutubeVideoPlayer()");
				}
				else
				{
					Security.allowDomain("www.youtube.com");
					if (stage)
					{
						init();
					}
					else
					{
						addEventListener(Event.ADDED_TO_STAGE, init);
					}
				}
		}
		public static function getInstance():YoutubeVideoPlayer
		{
			if (YoutubeVideoPlayer.instance == null)
			{
				YoutubeVideoPlayer.allowInstantiation = true;
				YoutubeVideoPlayer.instance = new YoutubeVideoPlayer();
				YoutubeVideoPlayer.allowInstantiation = false;
			}
			return YoutubeVideoPlayer.instance;
		}
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			initialized = false;
			stopRequest = false;
			ui = new YoutubeVideoPlayerUI();
			addChild(ui);
			youtubeLoader = new Loader();
			youtubeLoader.load(new URLRequest(API_PLAYER_URL));
			youtubeLoader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit);
		}
		private function onRemoved(e:Event = null):void {
			trace("video removed");
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			stop();
		}
		private function onAdded(e:Event = null):void {
			trace("video added");
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		private function onLoaderInit(e:Event):void {
			youtubeLoader.contentLoaderInfo.removeEventListener(Event.INIT, onLoaderInit);
			//totalTimeTxt.text = "0:00/0:00";
			addChild(youtubeLoader);
			alignUI();
			youtubeLoader.content.addEventListener("onReady", onPlayerReady);
			youtubeLoader.content.addEventListener("onStateChange", onStateChange);
			youtubeLoader.content.addEventListener("onError", onPlayerError);
			youtubeLoader.content.addEventListener("onPlaybackQualityChange", onVideoPlaybackQualityChange);
		}
		function onPlayerError(event:Event):void {
			// Event.data contains the event parameter, which is the error code
			trace("player error:", Object(event).data);
		}
		function onVideoPlaybackQualityChange(event:Event):void {
			// Event.data contains the event parameter, which is the new video quality
			trace("video quality:", Object(event).data);
		}
		private function alignUI():void {
			addChild(ui);
			ui.y = (_height);
			ui.x = 0.5 * (_width - ui.width);
		}
		private function onPlayerReady(e:Event):void {
			youtubeLoader.content.removeEventListener("onReady", onPlayerReady);
			youtubePlayer = youtubeLoader.content;
			initialized = true;
			youtubePlayer.setSize(width, width/ratio);
			if (source) {
				youtubePlayer.cueVideoById(source, 0);
			}
			//youtubePlayer.cueVideoById(urlCode, 0);//to get this id right click on the video /  copy video url / find video id in the last part of url address
			//addEventListener(Event.ENTER_FRAME, updatePlayer);
		}
		private function onStateChange(e:Event):void {
			if (initialized) {
				if (stopRequest) {
					//youtubePlayer.stopVideo();
					//stopRequest = false;
				}
			}
		}
		override public function get width():Number 
		{
			return _width;
		}
		
		override public function set width(value:Number):void 
		{
			_width = value;
			if (initialized) {
				_height = _width / ratio;
				youtubePlayer(_width, _height);
				alignUI();
			}
		}
		
		override public function get height():Number 
		{
			return _height;
		}
		
		override public function set height(value:Number):void 
		{
			_height = value;
			if (initialized) {
				youtubePlayer(_height * ratio, _height);
				alignUI();
			}
		}
		public function stop():void {
			stopRequest = true;
			//trace("stopping!!");
			if (initialized && _source != "") {
				//youtubePlayer.stopVideo();
				youtubePlayer.pauseVideo();
				//_source = "";
			}
		}
		public function get ratio():Number 
		{
			return _ratio;
		}
		
		public function set ratio(value:Number):void 
		{
			if (value == AspectRatio.NORMAL || value == AspectRatio.WIDE) {
				_ratio = value;
				width = _width;
			}else {
				throw new Error("Provided aspect ration is not valid use AspectRatio.NORMAL or AspectRatio.WIDE instead");
			}
			
		}
		
		public function get source():String 
		{
			return _source;
		}
		
		public function set source(value:String):void 
		{
			_source = value;
			if (initialized) {
				stopRequest = false;
				youtubePlayer.cueVideoById(_source, 0);
			}
		}
	}

}