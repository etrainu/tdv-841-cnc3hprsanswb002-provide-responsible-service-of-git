﻿package com.etrainu.ras_nsw 
{
	import com.etrainu.ras_nsw.RasModel;
	import com.etrainu.ras_nsw.GlobalParams;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.text.TextFormat;
	import flash.ui.ContextMenu;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import com.etrainu.MVC.*;
	import Utils.*;
	import Utils.Containers.*;
	import fl.managers.StyleManager;
	import flash.filters.BitmapFilterQuality;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuBuiltInItems;
	import flash.ui.ContextMenuItem;
	
	import flash.events.ContextMenuEvent;	
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.ApplicationDomain;
	public class ShellDocumentClass extends MovieClip
	{
		public var container:MovieClip;
		private var _global:Global;
		private var _model:RasModel;
		private var transBitmap:SmartSizedBitmap;
		private var containerSheild:SmartSizedSprite;
		private var containerStatus:String;
		private var format:TextFormat;
		private var target:Sprite;
		
		public function ShellDocumentClass()
		{
			GlobalParams.buildParams();
			_global = Global.getInstance();
			Security.loadPolicyFile(_global.crossdomainXMLUrl);
			var context:LoaderContext = new LoaderContext();
			context.checkPolicyFile = true;
			context.applicationDomain = ApplicationDomain.currentDomain;
			_model = RasModel.getInstance();
			format = new TextFormat();
			format.font="Arial";
			format.color = 0x1B1B1B;
			format.size=15;
			format.align = "right";
			StyleManager.setStyle("left", format);
			removeDefaultItems();
			addCustomMenuItems();
			this.contextMenu = _model.doteesContextMenu;
			var domain:String = this.loaderInfo.loaderURL;
			trace("url"+domain);
			if(domain){
				if(domain.indexOf("/")){
					domain = domain.slice(0, domain.lastIndexOf("/")+1);
				}else if(domain.indexOf("\\")){
					domain = domain.slice(0, domain.lastIndexOf("\\")+1);
				}
				_global.contentXmlPath = domain+_global.contentXmlPath;
				_global.ScoFolderPath = domain+_global.ScoFolderPath;
			}
			addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
		}
		private function init(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			_global.root = this;
			_global.stage = stage;
			transBitmap = new SmartSizedBitmap();
			transBitmap.autoStretch = true;
			transBitmap.autoDistruct = true;
			containerSheild = new SmartSizedSprite();
			containerSheild.autoStretch = true;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.RESIZE, stageResizeHandler, false, 0, true);
			stageResizeHandler();
		}
		public function disableContainer(immediate:Boolean = false, keepOnStage:Boolean = false):void {
			trace("Freezing the app...");
			containerStatus = "disabled";
			trace("this.getChildAt(0): "+this.getChildAt(0));
			target = Sprite(this.getChildAt(0));
			trace("currentChild: "+this.getChildAt(0));
			target.mouseEnabled = false;
			containerSheild.alpha = 1;
			containerSheild.graphics.clear();
			containerSheild.graphics.beginFill(_global.disabledContainerTintColor);
			containerSheild.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			//containerSheild.name = "containerSheild";
			containerSheild.alpha = _global.disabledContainerTintAmount;
			transBitmap = GetBitmap.draw(target, stage.stageWidth, stage.stageHeight);
			transBitmap.autoStretch = true;
			if (target.stage && !keepOnStage) {
				removeChild(target);
			}
			
			
			TweenMax.killTweensOf(target, true);
			TweenMax.killTweensOf(transBitmap, true);
			TweenMax.killTweensOf(containerSheild, true);
			if (!immediate) {
				TweenMax.to(transBitmap, _global.transitionDuration, { blurFilter: { blurX:_global.disabledContainerBlurAmount, blurY:_global.disabledContainerBlurAmount, quality:BitmapFilterQuality.LOW }, ease:Cubic.easeOut } );
				//TweenMax.to(target, _global.transitionDuration, { blurFilter: { blurX:_global.disabledContainerBlurAmount, blurY:_global.disabledContainerBlurAmount, quality:BitmapFilterQuality.LOW }, ease:Cubic.easeOut } );
				TweenMax.from(containerSheild, _global.transitionDuration, {alpha:0, ease:Cubic.easeOut, onComplete:containerDisabled});
			}else {
				TweenMax.to(transBitmap, 0.1, {blurFilter: { blurX:_global.disabledContainerBlurAmount, blurY:_global.disabledContainerBlurAmount, quality:BitmapFilterQuality.LOW},ease:Cubic.easeOut});
			}
			addChildAt(transBitmap, 0);
			addChildAt(containerSheild, 1);
		}
		public function enableContainer(immediate:Boolean = false):void {
			trace("Melting the app...");
			if (containerStatus != "enabled") {
				TweenMax.killTweensOf(target, true);
				TweenMax.killTweensOf(transBitmap, true);
				TweenMax.killTweensOf(containerSheild, true);
				if(!immediate){
					target.mouseEnabled = true;
					TweenMax.to(transBitmap, _global.transitionDuration, {blurFilter: { blurX:0, blurY:0 ,remove:true},ease:Cubic.easeOut});
					TweenMax.to(containerSheild, _global.transitionDuration, { autoAlpha:0 , ease:Cubic.easeOut, onComplete:containerEnabled } );
				}else {
					containerEnabled();
				}
			}
		}
		private function containerDisabled():void {
			trace("container Disabled");
			//removeChild(containerSheild);
		}
		private function containerEnabled():void {
			trace("container Enabled");
			transBitmap.filters = null;
			if (transBitmap.stage) {
				removeChild(transBitmap);
			}
			if (containerSheild.stage) {
				removeChild(containerSheild);
			}
			transBitmap = null;
			containerSheild.graphics.clear();
			target.filters = null;
			addChildAt(target, 0);
			containerStatus = "enabled";
		}
		
		//Context Menu
		protected function removeDefaultItems():void {
            _model.doteesContextMenu.hideBuiltInItems();
            var defaultItems:ContextMenuBuiltInItems = _model.doteesContextMenu.builtInItems;
            defaultItems.print = true;
			defaultItems.zoom = false;
        }
		
        protected function addCustomMenuItems():void {
			
			
			var developersNameItem:ContextMenuItem = new ContextMenuItem(_global.doteesDeclaration);
			developersNameItem.enabled = true;
            _model.doteesContextMenu.customItems.push(developersNameItem);
			developersNameItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, menuSelectHandler);
			
			var softwareNameItem:ContextMenuItem = new ContextMenuItem(_global.softwareName);
			softwareNameItem.enabled = true;
            _model.doteesContextMenu.customItems.push(softwareNameItem);
			softwareNameItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, menuSelectHandler);
        }
        protected function menuSelectHandler(event:ContextMenuEvent):void {
            navigateToURL(new URLRequest(_global.doteesContactMail), "_blank");
        }
		
		//Event Handlers
		private function freezApp(evt:Event):void {
			trace("Freezing Started");
			disableContainer();
		}
		private function meltApp(evt:Event):void {
			trace("Melting Started");
			enableContainer()
		}
		protected function stageResizeHandler(event:Event = null):void {
			var w:uint = 900;
			var h:uint = 600;
			var ratio:Number = 1;
			trace("stage.stageWidth: "+stage.stageWidth);
			trace("stage.stageHeight: "+stage.stageHeight);
			if (stage.stageWidth < w || stage.stageHeight < h) {
				var ratioX:Number = w / stage.stageWidth;
				var ratioY:Number = h / stage.stageHeight;
				
				ratio = Math.max(ratioX, ratioY);
				trace("ratioX: "+ratioX);
				trace("ratioY: "+ratioY);
				trace("ratio: "+ratio);
				container.scaleX = container.scaleY = 1/ratio;
			}
			container.x = 0.5*(stage.stageWidth-(w/ratio));
			container.y = 0.5*(stage.stageHeight-(h/ratio));
		}
	}
}