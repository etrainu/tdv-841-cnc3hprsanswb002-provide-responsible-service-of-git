﻿package com.etrainu.ras_nsw
{
	//import dotees.aryasasolHSE.Assesment.QuizEvent;

	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.utils.Timer;
	import Utils.*;
	import com.etrainu.MVC.*;
	import com.etrainu.ras_nsw.views.slideview.Services.*;
	import com.yahoo.astra.fl.managers.AlertManager;
	import com.adobe.serialization.json.JSON;
	import flash.display.DisplayObject;
	import flash.net.SharedObject;
	import flash.display.StageDisplayState;
	import source.courses.Scorm;
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class RasModel extends BaseModel implements IModel {
		private static var instance:RasModel = null;
		private static var allowInstantiation:Boolean = false;
		private const PROGRESS_PREVENTION_DURATION:uint = 1000;//millieseconds
		public var rootLevel:DisplayObject;
		public var lmsConnected:Boolean = false;
		public var jumpFrame:uint = 1;
		private var servicesInitialized:Boolean = false;
		private var _popUp:PopUp;
		
		
		//private var scormManager:SCORMManager;
		public var sharedObjectDB:SharedObject;
		public var metaRefrenceList:Array;
		
		private var global:Global;
		private var sessionStr:String;
		private var sessionData:Array;
		public var midtestFaliurs:uint = 0;
		private var highestIndex:int = -1;
		private var quizResults:Array;
		
		
		private var _acceptedCurrentIndex:int;
		public function RasModel() {
			if (RasModel.allowInstantiation) {
				global = Global.getInstance();
			}else {
				throw new Error("You may not directly instantiate the Model class!!\n Try using getInstance() method instead");
			}
		}
		public static function getInstance():RasModel {
			if ( RasModel.instance == null ) {
				RasModel.allowInstantiation = true;
				RasModel.instance = new RasModel();
				RasModel.allowInstantiation = false;
			}
			return RasModel.instance;
		}
		public function nextItem():void {
			if (currentIndex == totalItems-1) {
				finishCourse();
			}else {
				if (checkIndex(currentIndex+1)) {
					currentIndex += 1;
				}
			}
		}
		public function prevItem():void {
			if (checkIndex(currentIndex-1)) {
				currentIndex -= 1;
			}
		}
		//Overrides
		override protected function updateData():void {
			if (!preventExecution) {
				preventExecution = true;
				var preventExecutionTimer:Timer = new Timer(PROGRESS_PREVENTION_DURATION, 1);
				preventExecutionTimer.addEventListener(TimerEvent.TIMER_COMPLETE, allawExecution, false, 0, true);
				preventExecutionTimer.start();
				this.dispatchEvent(new Event(BaseModel.APP_HALT));
				currentScoData = getSCODataById(currentIndexId);
				_currentIndex = Number(currentScoData.@globalqueue-1);
				this.dispatchEvent(new Event(BaseModel.MODEL_ITEM_CHANGE));
			}
		}
		override protected function dataLoaded(event:Event):void {
			data = new XML(loader.data as String);
			data = bakeXmlData(data);
			scoList = data..SCO;
			
			totalItems = scoList.length();
			var msg:String = "";
			//dispatchPackageInitialization();
			currentIndex = 0;
		}
		override protected function dispatchPackageInitialization():void {
			evt = new Event(SCO_DATA_LOADED, true);
			initSession();
			this.dispatchEvent(evt);
		}
		private function allawExecution(event:TimerEvent):void {
			preventExecution = false;
		}
		private function initSession(event:TimerEvent = null):void {
			//readSession();
		}
		
		private function readSession():void {
			//this.dispatchEvent(new CustomEvent(CustomEvent.ON_MELT_REQUEST , { immediate : true }, true, false))
			sessionData = new Array();
			quizResults = new Array();
			sharedObjectDB = SharedObject.getLocal(global.doteesDeclaration+"_"+global.softwareName, "/");
			sessionStr = sharedObjectDB.data.sessionStr;
			//trace("sessionStr: "+sessionStr);
			//AlertManager.createAlert(global.root, "sessionStr: " + sessionStr, global.companyTitle, [global[global.courseLanguage + "String15"]]);
			trace("Reading Session: "+sessionStr);
			if (sessionStr && sessionStr != "") {
				sessionData = JSON.decode(sessionStr);
				quizResults = sessionData[0];
				//trace("quizResults: "+quizResults);
				midtestFaliurs =  Number(sessionData[1].midtestFaliurs);
				highestIndex = Number(sessionData[1].highestIndex);
				currentIndexId =  sessionData[1].currentIndexId;
			}else {
				trace("no previous session...");
				trace("data.SCOList[0].SCO[0].@id: "+data.SCOList[0].Chapter[0].SCO[0].@id);
				currentIndexId = data.SCOList[0].Chapter[0].SCO[0].@id;
			}
		}
		override public function saveSession(addProgress:Boolean = true):void {
			/*var tempObj:Object = new Object();
			//AlertManager.createAlert(global.root, "currentIndex: " + currentIndex+"\n"+"highestIndex: "+highestIndex, global.companyTitle, [global[global.courseLanguage + "String15"]]);
			if(addProgress){
				highestIndex = Math.max(highestIndex, currentIndex);
			}
			trace("step 1");
			tempObj = { midtestFaliurs:midtestFaliurs, currentIndexId:currentIndexId , highestIndex:highestIndex };
			trace("step 2");
			sessionData[0] = quizResults;
			trace("step 3");
			sessionData[1] = tempObj;
			trace("step 4");
			trace("quizResults: "+quizResults);
			sessionStr = JSON.encode(sessionData);
			//AlertManager.createAlert(global.root, "saving sessionStr: " + sessionStr, global.companyTitle, [global[global.courseLanguage + "String15"]]);
			sharedObjectDB.data.sessionStr = sessionStr;
			sharedObjectDB.flush();*/
		}
		private function checkIndex(index:uint):Boolean {
			var isAcceptable:Boolean = false;
			if (index >= 0 && index < totalItems) {
				isAcceptable = true;
			}
			return isAcceptable
		}
		private function checkIndexGrant(index:uint):Boolean {
			var isGranted:Boolean = false;
			if (index <= highestIndex+1) {
				isGranted = true;
			}
			return isGranted
		}
		private function finishCourse():void {
			//var totalScore:Number = 0;
			//for (var i:int = 0; i < quizResults.length; i++) {
			//	totalScore += quizResults[i];
			//}
			AlertManager.createAlert(global.root, global.courseFinishMsg);
			Scorm.CourseCompleted();
		}
		public function requirePopService(serviceClassName:Class):void {
			trace("requirePopService");
			//this.dispatchEvent(new Event(BaseModel.APP_HALT));
			initSevices();
			var popContent:MovieClip = new serviceClassName() as MovieClip;
			this.dispatchEvent(new Event(BaseModel.APP_FREEZ));
			_popUp.autoSize = false;
			_popUp.resizeFor(505, 225);
			popContent.init();
			_popUp.content = popContent;
			Align.alignToCenter(_popUp);
			global.root.addChild(_popUp);
		}
		private function initSevices():void {
			if (!servicesInitialized) {
				servicesInitialized = true;

				_popUp = new PopUp();
				_popUp.addEventListener(Event.CLOSE, onPopClose, false, 0, true);
			}
		}
		private function onPopClose(event:Event):void {
			global.root.removeChild(_popUp);
			this.dispatchEvent(new Event(BaseModel.APP_MELTDOWN));
		}
		//Getters & Setters
		override public function set currentIndexId(indexId:String):void {
			trace("currentIndexId: " + indexId);
			if(!preventExecution){
				_currentIndexId = indexId;
				updateData();
			}
		}
		override public function get currentIndexId():String {
			return _currentIndexId;
		}
	}

}