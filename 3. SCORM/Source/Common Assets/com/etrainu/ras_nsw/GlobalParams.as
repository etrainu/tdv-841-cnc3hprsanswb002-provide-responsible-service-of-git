﻿package com.etrainu.ras_nsw 
{
	import Utils.Global;
	/**
	 * ...
	 * @author ...
	 */
	public class GlobalParams 
	{
		private static var global:Global;
		public function GlobalParams() 
		{
			
		}
		public static function buildParams():void {
			global = Global.getInstance();
			
			
			
			global.disabledContainerTintColor = 0xFFFFFF;
			global.buttonTextColor = 0xCCCCCC;
			global.disabledContainerTintAmount = 0.4;
			global.disabledContainerBlurAmount = 7;
			global.transitionDuration = 1;
			global.companyTitle = "Responsible Service of Alcohol";
			global.softwareName = global.companyTitle;
			global.doteesDeclaration = "etrainu.com";
			global.doteesContactMail = "http://www.etrainu.com/contact.cfm";
			global.crossdomainXMLUrl = "http://etrainu.lms.sydney.s3-ap-southeast-2.amazonaws.com/crossdomain.xml";
			//global.contentXmlPath = "content/xml/Shell.xml";
			global.contentXmlPath = "content/xml/Shell.xml";
			global.XmlLabelFieldBase = "title";
			global.ScoFolderPath = "content";
			global.metaFileExtension = "met";
			global.thumbExtention = ".jpg";
			
			global.playerUpdateInterval = 500;
			global.sessionSaveInterval = 5;
			global.jumpSpeed = 3;
			global.midTestSCOName = "quiz";
			global.questionIconURL = "Assets/qMark.png";
			
			global.fullScreenMsg = "Do you want to start this course in fullscreen mode?";
			global.courseFinishMsg = "You have completed this stage.\rPlease continue onto the next stage.";
		}
	}

}