package com.etrainu.ras_nsw.views.slideview {
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import com.etrainu.ras_nsw.views.slideview.SlideDot;
	import com.etrainu.ras_nsw.RasModel;
	import com.etrainu.MVC.*;
	import flash.events.Event;
	public class Slides extends MovieClip {
		private const gap:int = 25;
		public var slideDot:SlideDot;
		public function Slides() {
			addEventListener(Event.ADDED_TO_STAGE, onAdded, false, 0, true);
		}
		public function setProgress(current:uint, total:uint, maxWidth:uint):void {
			clear();
			var xBase:int = 0;
			var yBase:int = 0;
			var slideDot:SlideDot;
			for (var i:int = 1; i <= total; i++ ) {
				slideDot = new SlideDot(i);
				slideDot.x = xBase;
				slideDot.y = yBase;
				if (i<=current) {
					slideDot.selected = true;
				}
				addChild(slideDot);
				xBase += gap;
				if (xBase > maxWidth) {
					xBase = 0;
					yBase += gap;
				}
			}
		}
		private function onAdded(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			clear();
			slideDot = null;
		}
		private function clear():void {
			var obj:DisplayObject;
			while (this.numChildren > 0) {
				obj = getChildAt(0);
				removeChild(obj);
				obj = null;
			}
		}
	}
	
}
