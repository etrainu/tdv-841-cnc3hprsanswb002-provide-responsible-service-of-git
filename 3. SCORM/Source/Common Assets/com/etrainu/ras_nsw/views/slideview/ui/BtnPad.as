package com.etrainu.ras_nsw.views.slideview.ui {
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import com.etrainu.ras_nsw.views.slideview.ui.SoundPanel;
	import com.etrainu.ras_nsw.RasModel;
	import com.etrainu.MVC.*;
	import flash.events.MouseEvent;
	import flash.events.Event;
	public class BtnPad extends MovieClip {
		public var nextItemBtn:SimpleButton;
		public var prevItemBtn:SimpleButton;
		public var soundPanel:SoundPanel;
		private var _model:RasModel;
		public function BtnPad() {
			addEventListener(Event.ADDED_TO_STAGE, onAdded, false, 0, true);
			prevItemBtn.useHandCursor = false;
			nextItemBtn.useHandCursor = false;
			nextItemBtn.addEventListener(MouseEvent.CLICK, onNextClick);
			prevItemBtn.addEventListener(MouseEvent.CLICK, onPrevClick);
		}
		private function onAdded(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			_model = RasModel.getInstance();
		}
		private function onNextClick(e:MouseEvent):void {
			trace("next");
			_model.nextItem();
		}
		private function onPrevClick(e:MouseEvent):void {
			trace("prev");
			_model.prevItem();
		}
	}
	
}
