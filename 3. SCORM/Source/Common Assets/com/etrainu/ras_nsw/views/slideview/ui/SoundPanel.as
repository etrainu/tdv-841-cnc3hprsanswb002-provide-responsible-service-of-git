﻿package com.etrainu.ras_nsw.views.slideview.ui {
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.SoundMixer;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	import flash.media.SoundTransform;

	public class SoundPanel extends MovieClip {
		private const RAIL_LENGTH:uint = 90;
		public var dragger:MovieClip;
		public var draggerBG:MovieClip;
		
		private var rail:Sprite;
		
		private var _volume:int;
		private var st:SoundTransform;
		public function SoundPanel() {
			dragger.mouseChildren = false;
			dragger.mouseEnabled = false;
			draggerBG.mouseChildren = false;
			draggerBG.mouseEnabled = false;
			st = new SoundTransform();
			addEventListener(Event.ADDED_TO_STAGE, onAdded, false, 0, true);
		}
		private function onAdded(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			rail = new Sprite();
			rail.graphics.beginFill(0xFFFFFF, 0);
            rail.graphics.drawRect(0, 0, this.width, RAIL_LENGTH);
            rail.graphics.endFill();
			rail.y = dragger.height * 0.5;
			addChild(rail);
			rail.addEventListener(MouseEvent.MOUSE_DOWN, onRailDown, false, 0, true);
		}
		private function onRailDown(e:MouseEvent):void {
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMove, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_UP, onUp, false, 0, true);
			onMove(e);
		}
		private function onMove(e:MouseEvent):void {
			var mousePos:Point = new Point(e.stageX, e.stageY);
			mousePos = rail.globalToLocal(mousePos);
			volume = ((RAIL_LENGTH-mousePos.y) / RAIL_LENGTH) * 100;
		}
		private function validateVolume(volume:int):int {
			if (volume>100) {
				volume = 100;
			}else if (volume<0) {
				volume = 0;
			}
			return volume;
		}
		private function onUp(e:MouseEvent):void {
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		
		public function get volume():int 
		{
			return _volume;
		}
		
		public function set volume(value:int):void 
		{
			_volume = validateVolume(value);
			TweenLite.killTweensOf(dragger);
			
			var draggerPos:int = RAIL_LENGTH*((100-_volume)/100);
			trace(draggerPos);
			TweenLite.to(dragger, 1, { y:draggerPos, ease:Expo.easeOut, delay:0.1 } );
			st.volume = _volume/100;
			SoundMixer.soundTransform = st;
		}
	}
	
}
