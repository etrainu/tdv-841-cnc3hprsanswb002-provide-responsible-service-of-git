﻿package com.etrainu.ras_nsw.views {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.net.navigateToURL;	
	import flash.net.URLRequest;
	import com.etrainu.ras_nsw.views.slideview.Slides;
	import com.etrainu.ras_nsw.views.slideview.ui.BtnPad;
	import com.etrainu.ras_nsw.RasModel;
	import com.etrainu.MVC.*;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	import Utils.*;
	
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.ApplicationDomain;
	
	public class Container extends MovieClip {
		public var btnPanel:BtnPad;
		public var bg:MovieClip;
		public var contentBG:MovieClip;
		public var courseLogo:MovieClip;
		public var Logo:MovieClip;
		public var slidesCount:Slides;
		public var titleTxt:TextField;
		private var _model:RasModel;
		private var _global:Global;
		private var content:Content;
		public function Container() {
			_model = RasModel.getInstance();
			_global = Global.getInstance();
			_model.addEventListener(BaseModel.MODEL_ITEM_CHANGE, onItemChange, false, 0, true);
			addEventListener(Event.ADDED_TO_STAGE, onAdded, false, 0, true);
			btnPanel.alpha = 0;
			contentBG.visible = false;
		}
		private function onAdded(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			contentBG.addEventListener(Event.COMPLETE, onTrarnsitionComplete, false, 0, true);
			contentBG.visible = true;
			contentBG.gotoAndPlay("transIn");
		}
		private function onTrarnsitionComplete(e:Event):void {
			btnPanel.alpha = 1;
			TweenLite.from(btnPanel, 1, { alpha:1, x:btnPanel.x - 70, ease:Back.easeOut , onComplete:loadContent} );
			content = new Content(contentBG.width, contentBG.height);
			content.x = contentBG.x;
			content.y = contentBG.y;
			
		}
		function loadContent():void {
			addChild(content);
			_model.addEventListener(BaseModel.SCO_DATA_LOADED, dataLoaded, false, 0, true);
			_model.load(new URLRequest(_global.contentXmlPath));
		}
		private function dataLoaded(e:Event):void {
			_model.removeEventListener(BaseModel.SCO_DATA_LOADED, dataLoaded);
		}
		private function onItemChange(e:Event):void {
			content.load(_global.ScoFolderPath + "/" + _model.currentScoData.@folderPath + "/" + _model.currentScoData.@file);
			slidesCount.setProgress(_model.currentIndex + 1, _model.totalItems, contentBG.width);
			alignSlideCount();
			updateTitle();
		}
		private function updateTitle():void {
			//titleTxt.text = _model.currentScoData.@title;
			TweenLite.killTweensOf(titleTxt);
			TweenLite.to(titleTxt, 1, { alpha:0, ease:Expo.easeOut, delay:0.1 , onComplete:newTitleIn} );
		}
		private function newTitleIn():void {
			TweenLite.killTweensOf(titleTxt);
			titleTxt.text = _model.currentScoData.@title;
			TweenLite.to(titleTxt, 1, { alpha:1, ease:Expo.easeOut} );
		}
		private function alignSlideCount():void {
			slidesCount.x = contentBG.x + contentBG.width - slidesCount.width - 10;
			var realestateHeight:int = bg.height - contentBG.y - contentBG.height;
			slidesCount.y = Math.min(contentBG.y + contentBG.height + ((realestateHeight - slidesCount.height) * 0.4), (bg.height - slidesCount.height));
		}
		
	}
	
}
