﻿package com.etrainu.ras_nsw.views 
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.ApplicationDomain;
	import com.etrainu.MVC.*;
	import Utils.*;
	import Utils.Containers.*;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.ApplicationDomain;
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class Content extends Sprite 
	{
		private var targetHeight:uint;
		private var targetWidth:uint;
		private var loader:Loader;
		public function Content(targetWidth:uint, targetHeight:uint) 
		{
			this.targetWidth = targetWidth;
			this.targetHeight = targetHeight;
			loader = new Loader();
			
		}
		public function load(url:String):void {
			trace("model requested " + url);
			loader.unloadAndStop();
			addChild(loader);
			var global:Global = Global.getInstance();
			Security.loadPolicyFile(global.crossdomainXMLUrl);
			var context:LoaderContext = new LoaderContext();
			context.checkPolicyFile = true;
			context.applicationDomain = ApplicationDomain.currentDomain;
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError, false, 0, true);
			loader.load(new URLRequest(url), context);
		}
		private function onError(e:IOErrorEvent):void {
			trace(e.text);
		}
	}

}