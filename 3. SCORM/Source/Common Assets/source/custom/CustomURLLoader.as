﻿package source.custom
{
	import flash.net.URLLoader;
	import flash.net.URLRequest;
/*
	Subclass of the default Flash URLLoader
	so that more variables can be added
*/ 
	public dynamic class CustomURLLoader extends URLLoader
	{
		public var required:Boolean = false;
		public var complete:Function = null;
	}
}