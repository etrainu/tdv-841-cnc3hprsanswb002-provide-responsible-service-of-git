﻿package source.general
{
	import source.custom.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.net.*;
	import flash.ui.*;

	public class GeneralPreloader
	{
		public static var url_loaders : Array = [];
		public static var image_loaders : Array = [];
		public static var load_sequentially : Boolean = false; // configure this
		public static var required_count : int = 0;
		public static var ready_handler : Function;
		public static var cache_buster : String = "";

		public static function LoadURL (url:String, required:Boolean=false, complete_fn:Function=null) : CustomURLLoader
		{
			var loader : CustomURLLoader = new CustomURLLoader;
			loader.required = required;
			loader.complete = complete_fn;
			if (required) required_count++;
			return LoadURLCore (loader, url);
		}
		public static function LoadURLCore (loader:CustomURLLoader, url:String) : CustomURLLoader
		{
			var request:URLRequest = new URLRequest (url + cache_buster);
			loader.loading = false;
			loader.loaded = false;
			url_loaders.push (loader);
			loader.addEventListener (Event.COMPLETE, URLLoaded);
			loader.addEventListener (IOErrorEvent.IO_ERROR , LoadError);
			try { loader.load(request);} catch (error:Error){ trace ("Unable to load requested URL: " + url);}
			return loader;
		}
		public static function URLLoaded (event:Event) : void
		{
			var loader : CustomURLLoader = CustomURLLoader (event.target);
			var index:int = url_loaders.indexOf (loader);
			if (loader.complete != null) loader.complete (loader);
			if (loader.required) CheckRequiredCount();
			url_loaders.splice (index, 1);
		}
		public static function LoadImage (url:String, required:Boolean=false, complete_fn:Function=null) : CustomLoader
		{
			//required = true; // force all images to preload
			var loader : CustomLoader = new CustomLoader;
			loader.url = url;
			loader.required = required;
			loader.complete = complete_fn;
			loader.loading = false;
			loader.loaded = false;
			image_loaders.push (loader);
			if (!load_sequentially) StartLoadingImage (loader);
			else if (image_loaders.length == 1) StartLoadingImage (loader);
			if (required) required_count++;
			return loader;
		}
		public static function LoadNextImage () : void
		{
			var loader:CustomLoader;
			for each (loader in image_loaders){
				if (loader.loading){
					return;}}

			// load required images first
			for each (loader in image_loaders){
				if (!loader.loading && !loader.loaded && loader.required){
					StartLoadingImage (loader);
					return;}}

			// load any remaining image
			for each (loader in image_loaders){
				if (!loader.loading && !loader.loaded){
					StartLoadingImage (loader);
					return;}}
		}
		public static function StartLoadingImage (loader:CustomLoader) : void
		{
			var request:URLRequest = new URLRequest (loader.url);
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, ImageLoaded);
			loader.contentLoaderInfo.addEventListener (IOErrorEvent.IO_ERROR, LoadError);
			loader.loading = true;
			try { loader.load (request);}
			catch (error:Error){ trace ("Unable to load requested URL");}
		}
		public static function ImageLoaded (event:Event) : void
		{
			var loader_info : LoaderInfo = LoaderInfo (event.target);
			var loader : CustomLoader = CustomLoader (loader_info.loader);
			var index:int = image_loaders.indexOf (loader);
			loader.loading = false;
			loader.loaded = true;
			if (loader.complete) loader.complete (loader_info); // calls handler
			if (loader.required) CheckRequiredCount();
			image_loaders.splice (index, 1);
			LoadNextImage ();
		}
		public static function PreloadStream (url:String, required:Boolean, complete_fn:Function) : CustomStream
		{
			var request:URLRequest = new URLRequest (url);
			var stream : CustomStream = new CustomStream();
			stream.addEventListener (Event.COMPLETE, StreamLoaded);
			stream.addEventListener (IOErrorEvent.IO_ERROR, LoadError);
			stream.required = required;
			stream.complete = complete_fn;
			try { stream.load(request);} catch (error:Error){ trace ("Unable to load requested URL");}
			if (required) required_count++;
			return stream;
		}
		public static function StreamLoaded (event:Event) : void
		{
			var stream : CustomStream = CustomStream (event.target);
			if (stream.complete) stream.complete (stream);
			if (stream.required) CheckRequiredCount ();
		}
		public static function CheckRequiredCount () : void
		{
			required_count--;
			if (required_count == 0){
				if (ready_handler != null){
					ready_handler();
					ready_handler = null;}}
		}
		public static function GetProgress () : Number
		{
			var total : Number = 1;
			var loaded : Number = 1;

			for each (var loader:Loader in image_loaders){
				loaded += loader.contentLoaderInfo.bytesLoaded;
				total += loader.contentLoaderInfo.bytesTotal;}

			for each (var url_loader:URLLoader in url_loaders){
				loaded += url_loader.bytesLoaded;
				total += url_loader.bytesTotal;}

			return loaded / total;
		}
		/* comment out the inappropriate choice */
		//public static var error_mode : String = "release";
		public static var error_mode : String = "debug";
		public static var error_message : String = "There was a problem loading a data file. Please reload the page.";
		public static function LoadError (event:Event) : void
		{
			trace (event.toString());
			//if (error_mode == "debug") Alert.show (event.toString());
			//else Alert.show (error_message);
		}
	}
}
