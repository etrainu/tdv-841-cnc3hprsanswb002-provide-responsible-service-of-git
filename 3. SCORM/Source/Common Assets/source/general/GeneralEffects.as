package source.general
{
	import source.custom.*;
	import flash.display.*

	public class GeneralEffects
	{
		public static var disable_effects:Boolean = false;

		public static function Set (sprite:*, effect_names:String):void
		{
			sprite.fade = 0;
			sprite.fade_down = 0;
			var effect_array:Array = effect_names.split (",");
			for each (var effect_name:String in effect_array){
				if (effect_name == "fade up"){
					sprite.fade = 0.25;
					sprite.alpha = 0;}
				else if (effect_name == "slow fade up"){
					sprite.fade = 0.2;
					sprite.alpha = 0;}
				else if (effect_name == "fade down"){
					sprite.fade_down = -0.25;}
				else if (effect_name == "slow fade down"){
					sprite.fade_down = -0.2;}}

			if (disable_effects){
				sprite.alpha = 1;
				return;}
		}
		public static function Update (container:Sprite, remove_sprite:Boolean):void
		{
			for (var i:int=container.numChildren-1; i>=0; i--)
			{
				var sprite:* = container.getChildAt(i);
				if (sprite.fade){
					sprite.alpha = Math.max (0, Math.min (1, sprite.alpha + sprite.fade));
					if (sprite.alpha == 1) sprite.fade = 0;
					if (remove_sprite && sprite.alpha == 0) container.removeChild (sprite);}
			}
		}
		public static function Hide (sprite:CustomSprite):void
		{
			if (sprite){
				sprite.fade = sprite.fade_down;
				if (disable_effects) sprite.alpha = 0;}
		}
		public static function HideAll (container:Sprite, id:String):void
		{
			for (var i:int=container.numChildren-1; i>=0; i--)
			{
				var sprite:* = container.getChildAt (i);
				if (sprite.identity == id){
					if (sprite.fade_down){
						sprite.fade = sprite.fade_down;
						if (disable_effects) sprite.alpha = 0;}
					else container.removeChild (sprite);}
			}
		}
	}
}