package source.general
{
	import source.custom.*;
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import flash.geom.*;

	public class GeneralButtons
	{
		public static var margin_x:Number = 8;
		public static var margin_y:Number = 8;
		public static var image_width:Number = 50;
		public static var image_height:Number = 70;

		public static function DrawButtonBase (button_width:Number, button_height:Number, do_image:Boolean=false) : CustomSprite
		{
			var round_radius:Number = 28;
			var sprite:CustomSprite = new CustomSprite;
			var gx:Graphics = sprite.graphics;
			gx.lineStyle (1, 0x404040, 1, true);
			var m:Matrix = new Matrix; m.createGradientBox (button_width, button_height, Math.PI/2);
			gx.beginGradientFill ("linear", [0xFFFFFF,0xF4F6FC,0xECF0F4,0xE0E4EC],[1,1,1,1],[0,55,200,255], m);
			gx.drawRoundRect (0, 0, button_width, button_height, round_radius, round_radius);
			if (do_image) gx.drawRoundRect (margin_x, margin_y, image_width, image_height, 16, 16);
			gx.endFill();
			sprite.filters = [
				new DropShadowFilter (1, 45, 0, 0.7, 6, 6, 1, 1),
				new BevelFilter (2, 45, 0xFFFFFF, 0.7, 0, 0.7, 4, 4)];
			return sprite;
		}
		public static function MakePictureButton (image_url:String, text:String, desc:String, button_width:Number=0) : CustomSprite
		{
			var sprite:CustomSprite = new CustomSprite;

			var image_sprite:CustomLoader = GeneralLoader.LoadSizedImage (image_url, 50, 70);
			image_sprite.x = margin_x;
			image_sprite.y = margin_y;
			sprite.addChild (image_sprite);

			var text_x:Number = image_width + margin_x * 2 + 5;
			var button_height:Number = image_height + margin_y*2;

			var text_field:Sprite = GeneralText.MakeText (text, 18, text_x, margin_y + 4, 0x507090);
			var desc_field:Sprite = GeneralText.MakeText (desc, 12, text_x, 0, 0x484860, button_width-80);
			desc_field.y = button_height/2 - desc_field.height/2 + 12;

			if (button_width == 0) button_width = image_width + text_field.width + margin_x*3;

			var button_base:Sprite = DrawButtonBase (button_width, button_height, true);
			sprite.addChild (button_base);

			sprite.addChild (text_field);
			sprite.addChild (desc_field);
			sprite.buttonMode = true;
			return sprite;
		}
	}
}
