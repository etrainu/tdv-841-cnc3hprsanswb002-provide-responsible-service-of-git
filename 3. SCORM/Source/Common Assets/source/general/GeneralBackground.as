package source.general
{
	import source.custom.*;
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import flash.geom.*;
	import flash.net.*;

	public class GeneralBackground extends Sprite
	{
		public var stage_width:Number;
		public var stage_height:Number;
		public var gradient_sprite:Sprite;
		public var background_image:Bitmap;

		public function Setup (w:Number, h:Number):void
		{
			stage_width = w;
			stage_height = h;
		}
		public function DrawGradient (colors_xml:XML) : void
		{
			gradient_sprite = new Sprite;
			var matrix:Matrix = new Matrix;
			matrix.createGradientBox (stage_width, stage_height, Math.PI/2, 0, 0);

			var colors:Array = GeneralConversion.GetGradientColors (colors_xml.background.gradient.@colors);
			var alphas:Array = GeneralConversion.GetGradientAlphas (colors_xml.background.gradient.@alphas);
			var ratios:Array = GeneralConversion.GetGradientRatios (colors_xml.background.gradient.@ratios);

			var g:Graphics = gradient_sprite.graphics;
        	g.beginGradientFill ("linear", colors, alphas, ratios, matrix, "pad", InterpolationMethod.RGB);
        	g.drawRect (0, 0, stage_width, stage_height);
			addChild (gradient_sprite);
		}
		public function LoadImage (url:String):CustomLoader
		{
			var loader:CustomLoader = new CustomLoader;
			var request:URLRequest = new URLRequest (url);
			loader.contentLoaderInfo.addEventListener (IOErrorEvent.IO_ERROR, GeneralLoader.LoadError);
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, ImageLoaded);
			try { loader.load (request);} catch (error:Error){ trace ("Unable to load requested URL");}
			return loader;
		}
        public function ImageLoaded (e:Event):void
        {
			var loader_info : LoaderInfo = LoaderInfo (e.target);
			var loader : Loader = Loader (loader_info.loader);
			var bitmap:Bitmap = loader.content as Bitmap;
			var bitmap_data:BitmapData = bitmap.bitmapData;
			background_image = new Bitmap (bitmap_data);
			ResizeImage ();
			addChild (background_image);
		}
		public function Resize (w:Number, h:Number):void
		{
			stage_width = w;
			stage_height = h;
			if (background_image)
				ResizeImage ();
		}
		public function ResizeImage ():void
		{
			var bitmap_data:BitmapData = background_image.bitmapData;
			if (bitmap_data.width == stage_width && bitmap_data.height == stage_height){
				background_image.width = stage_width;
				background_image.height = stage_height;
				background_image.smoothing = false;}
			else {
				var stage_ratio:Number = stage_width / stage_height;
				var image_ratio:Number = bitmap_data.width / bitmap_data.height;
				if (image_ratio > stage_ratio){
					background_image.width = stage_width * image_ratio / stage_ratio;
					background_image.height = stage_height;
					background_image.smoothing = true;}
				else if (image_ratio < stage_ratio){
					background_image.width = stage_width;
					background_image.height = stage_height / image_ratio * stage_ratio;
					background_image.smoothing = true;}
				else {
					background_image.width = stage_width;
					background_image.height = stage_height;
					background_image.smoothing = true;}}
		}
        public function UnloadImage ():void
        {
			if (background_image)
				removeChild (background_image);
        }
	}
}
