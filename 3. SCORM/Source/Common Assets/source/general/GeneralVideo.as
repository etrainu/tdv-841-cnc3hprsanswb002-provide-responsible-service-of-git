package source.general
{
	import flash.display.*;
	import flash.events.*;
	import flash.media.*;
	import flash.net.*;

	public class GeneralVideo
	{
		public static var video_sprite:Video;
		public static var net_connection:NetConnection;
		public static var net_stream:NetStream;
		public static var url_array:Array = [];
		public static var url_index:int = 0;
		public static var playing:Boolean = false;
		public static var paused:Boolean = false;
		public static var auto_play:Boolean = false;
		public static var function_start:Function;
		public static var function_finish:Function;

		public static function Play (urls:Array, _auto:Boolean=false, fn_start:Function=null, fn_finish:Function=null):Video
		{
			if (playing) Stop();
			auto_play = _auto;
			url_array = urls;
			url_index = 0;
			function_start = fn_start;
			function_finish = fn_finish;
			Setup ();
			PlayNext ();
			return video_sprite;
		}
		public static function Setup ():void
		{
			video_sprite = new Video;
			net_connection = new NetConnection;  
			net_connection.connect (null);
			net_stream = new NetStream (net_connection);
			video_sprite.attachNetStream (net_stream);
			var listener:Object = new Object();  
			listener.onMetaData = OnMetaData;
			net_stream.client = listener;
			net_stream.addEventListener (NetStatusEvent.NET_STATUS, OnNetStatus);
		}
		public static function Resume ():void
		{
			if (paused){
				net_stream.resume();
				paused = false;}
		}
		public static function PlayNext ():void
		{
			if (url_index < url_array.length){
				var video_url:String = url_array[url_index++];
				net_stream.play (video_url);
				playing = true;}
			else {
				if (function_finish != null)
					function_finish();
				Stop ();}
		}
		public static function OnNetStatus (event:NetStatusEvent):void
		{
			if (event.info.code == "NetStream.Play.Start"){
				if (function_start != null)
					function_start (video_sprite);
				if (!auto_play){
					net_stream.pause();
					paused = true;}}
			if (event.info.code == "NetStream.Play.Stop"){
				PlayNext();}
		}
		public static function OnMetaData (evt:Object):void
		{

		}
		public static function Stop ():void
		{
			if (net_stream){
				net_stream.removeEventListener (NetStatusEvent.NET_STATUS, OnNetStatus);
				net_stream.close();}

			function_start = null;
			function_finish = null;
			playing = false;
		}
/*		
		Use the StageWebView class to play video on ipad with native player controls.
		but you'r not able to control the player with your scripts.
		The below code will work with native players.

		import flash.geom.Rectangle;
		import flash.media.StageWebView;
		import flash.filesystem.File;
		import flash.utils.Timer;
		import flash.events.TimerEvent;

		stop();

		var webView:StageWebView = new StageWebView();
		webView.stage = this.stage;
		webView.viewPort = new Rectangle(0,0,stage.stageWidth,stage.stageHeight);
		var fPath:String = new File(new File("app:/MyMovie.mp4").nativePath).url;
		webView.loadURL( fPath );

		To know the end of the video, Just create an timer and activate the event when equal to the video timing (manually find that) and use the below code to remove the video:

		webView.stage = null
*/	
	}
}
