package source.general
{
	public class GeneralString
	{
		public static function Resolve (input:String):String
		{
			var output:String = input.replace ("<sup>2</sup>","&#178;")
									 .replace ("<sup>3</sup>","&#179;")
									 .replace ("<sup>","<font face=\"GG Superscript\">")
									 .replace ("</sup>","</font>")
									 .replace ("<sub>","<font face=\"GG Subscript\">")
									 .replace ("<red>","<font color=\"#800000\">")
									 .replace ("<green>","<font color=\"#007000\">")
									 .replace ("<blue>","<font color=\"#000090\">")
									 .replace ("<link>","<font color=\"#000090\"><u>")
									 .replace ("</link>","</u></font>")
									 .replace ("</red>","</font>")
									 .replace ("</green>","</font>")
									 .replace ("</blue>","</font>")
									 .replace ("</sub>","</font>");
			return output;
		}
		public static function Trim (s:String):String
		{
			return TrimLeft ( TrimRight(s) );
		}
		public static function TrimLeft (s:String):String
		{
			if (s == null) return "";

			var size:int = s.length;
			for (var i:int = 0; i < size; i++){
				if (s.charCodeAt(i) > 32){
					return s.substring(i);}}
			return "";
		}
		public static function TrimRight (s:String):String
		{
			if (s == null) return "";

			var size:int = s.length;
			for (var i:int = size; i > 0; i--){
				if (s.charCodeAt(i - 1) > 32)
					return s.substring(0, i);}
			return "";
		}
		public static function isEmpty(s:String):Boolean
		{
			return (s == null) || (s.length == 0);
		}
		public static function isSpace(c:int):Boolean
		{
			return (c <= 32);
		}
		public static function isAlpha(c:String):Boolean
		{
			return ( ("A" <= c) && (c <= "Z") ) || ( ("a" <= c) && (c <= "z") );
		}
		public static function isDigit(c:String):Boolean
		{
			return ("0" <= c) && (c <= "9");
		}
		public static function isHexDigit( c:String ):Boolean
		{
			return isDigit(c) || ( ("A" <= c) && (c <= "F") ) || ( ("a" <= c) && (c <= "f") );
		}
		public static function isValidIdentifierStartChar (c:String):Boolean
		{
			return (c == "_") || isAlpha(c);
		}
		public static function isIdentifierChar(c:String):Boolean
		{
			return (c == "_") || isAlpha(c) || isDigit(c);
		}
	}
}
