package source.general
{
	import flash.net.SharedObject;
	
	public class GeneralConfig
	{
		public static var so:SharedObject;

		public static function Initialize ():void
		{
			if (!so) so = SharedObject.getLocal ("config");
		}
		public static function Get (name:String, defval:Object):Object
		{
			Initialize ();
			if (so.data.hasOwnProperty (name))
				return so.data[name];
			return defval;
		}
		public static function Set (name:String, value:Object):void
		{
			Initialize ();
			so.data[name] = value;
			so.flush();
		}
	}
}
