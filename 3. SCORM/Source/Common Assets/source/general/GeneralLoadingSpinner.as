package source.general
{
	import mx.core.*;
	import mx.controls.*;
	import flash.events.*;
	import flash.display.*;
	import flash.text.*;
/*
	Flex component that shows a spinning pie with text
	to indicate that files are loading
*/
	public class GeneralLoadingSpinner extends UIComponent
	{
		public var spinner_text : TextField = new TextField;
		public var spinner_sprite : Sprite = new Sprite;
		public var spinner_alpha : Number = -0.5;
		public var slow_progress : Number = 0.01;

		public function GeneralLoadingSpinner()
		{
			var segs:Number = 20;
			var max:Number = 360;
			var inc:Number = max / segs;
			var alp:Number = 0.8;

			spinner_sprite = new Sprite;

			for (var ang:Number=0; ang < max; ang += inc){
				if (alp > 0){
					var sprite:Sprite = new Sprite;
					sprite.graphics.beginFill (0x808080);
					sprite.graphics.moveTo (-1.0  ,10);
					sprite.graphics.lineTo (-4  ,30);
					sprite.graphics.lineTo (4  ,30);
					sprite.graphics.lineTo (1.0  ,10);
					sprite.graphics.lineTo (-1.0  ,10);
					sprite.graphics.endFill ();
					sprite.rotation = ang;
					sprite.alpha = alp;
					alp -= 1.0 / segs;
					spinner_sprite.addChild (sprite);}}

			spinner_sprite.x = 0;
			spinner_sprite.y = 30;
			spinner_sprite.addEventListener (Event.ENTER_FRAME, OnSpin);
			spinner_sprite.scaleX = 0.5;
			spinner_sprite.scaleY = 0.5;
			addChild (spinner_sprite);

			var text_format : TextFormat = new TextFormat;
			text_format.size = 18;
			text_format.font = "DIN";
			text_format.color = 0x808080;
			spinner_text = new TextField;
			spinner_text.autoSize = "left";
			spinner_text.mouseEnabled = false;
			spinner_text.selectable = false;
			spinner_text.embedFonts = true;
			spinner_text.defaultTextFormat = text_format;
			spinner_text.text = "Loading ...";
			spinner_text.x = 30;
			spinner_text.y = 20;
			addChild (spinner_text);

			// Set the flex component size
			minWidth = 200;
			minHeight = 60;
		}
/*
		Synthesizes a dodgy loading percentage value that counts slowly upwards
		This is required because the total number of bytes keeps growing
*/
		public function SetProgress (progress:Number):void
		{
			if (progress > slow_progress){
				slow_progress += (progress - slow_progress)/40 + 0.0005;
				spinner_text.text = "Loading ... " + (slow_progress * 100).toFixed(0) + "%";}
		}

		public function OnSpin (e:Event):void
		{
			if (visible){
				spinner_sprite.rotation -= 10;}
		}
	}
}