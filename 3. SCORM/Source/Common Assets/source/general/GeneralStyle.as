package source.general
{
	public class GeneralStyle
	{
		public static function GetStyle (map:Object, property:String, default_value:*):*
		{
			if (map.hasOwnProperty (property)){
				var value:String = map[property];
				value = value.replace ("px","");
				return value;}
			return default_value;
		}
		public static function SetStyle (map:Object, property:String, value:*):*
		{
			return map[property] = value;
		}
		public static function StringToMap (style:String):Object
		{
			var map:Object = new Object;
			var list:Array = style.split(";")
			for (var i:int = 0; i < list.length; i++){
				var pair:Array = list[i].split(":", 2);
				if (pair.length != 2) continue;
				var name:String = GeneralString.Trim (pair[0]).toLowerCase();
				if (name.length == 0) continue;
				map[name] = GeneralString.Trim (pair[1]);}
			return map;
		}
		public static function MapToString (map:Object):String
		{
			var string:String = "";
			for (var property:String in map){
				if (string.length) string += "; ";
				string += property + ":" + map[property];}
			return string;
		}
	}
}
