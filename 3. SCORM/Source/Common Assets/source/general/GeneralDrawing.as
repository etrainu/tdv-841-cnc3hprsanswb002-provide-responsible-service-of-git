package source.general
{
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import flash.geom.*;
	import flash.text.*;

	public class GeneralDrawing
	{
		public static const to_radians:Number = Math.PI / 180;

		public static function ArcTo (
			g : Graphics,
			x : Number, 
			y : Number,
			xradius : Number,
			yradius : Number,
			arc_angle : Number,
			start_angle : Number) : void
		{
			var segs:Number = Math.ceil (Math.abs (arc_angle) / 5);
			var seg_angle:Number = arc_angle / segs;
			var theta:Number = -seg_angle * to_radians;
			var angle:Number = -start_angle * to_radians;
			var ax:Number = x - Math.cos(angle) * xradius;
			var ay:Number = y - Math.sin(angle) * yradius;

			if (segs > 0) {
				//target.moveTo (x,y);
				for (var i:Number = 0; i<segs; i++)
				{
					angle += theta;
					var mid_angle:Number = angle - (theta/2);
					var bx:Number = ax + Math.cos(angle) * xradius;
					var by:Number = ay + Math.sin(angle) * yradius;
					var cx:Number = ax + Math.cos(mid_angle) * (xradius/Math.cos(theta/2));
					var cy:Number = ay + Math.sin(mid_angle) * (yradius/Math.cos(theta/2));
					g.curveTo (cx, cy, bx, by);
				}
			}
		}

		public static function DrawPartRoundRect (
			g:Graphics,
			x:Number, y:Number,
			w:Number, h:Number,
			r1:Number, r2:Number,
			r3:Number, r4:Number) : void
		{
			g.moveTo (x + r1, y);
			g.lineTo (x + w - r2, y);
			if (r2) ArcTo (g, x + w - r2, y, r2, r2, -90, -270);
			else { g.lineTo (x + w, y); g.lineTo (x + w, y); }
			g.lineTo (x + w, y + h - r3);
			if (r3) ArcTo (g, x + w, y + h - r3, r3, r3, -90, 0);
			else { g.lineTo (x + w, y + h); g.lineTo (x + w, y + h); }
			g.lineTo (x + r4, y + h);
			if (r4) ArcTo (g, x + r4, y + h, r4, r4, -90, -90);
			else { g.lineTo (x, y + h); g.lineTo (x, y + h); }
			g.lineTo (x, y + r1);
			if (r1) ArcTo (g, x, y + r1, r1, r1, -90, -180);
			else { g.lineTo (x, y); g.lineTo (x, y); }
		}
	}
}


