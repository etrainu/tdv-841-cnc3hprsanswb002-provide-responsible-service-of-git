package source.general
{
	import flash.display.*;
	import flash.geom.*;
	import flash.text.*;
	
	import source.custom.*;

	public class GeneralBrowser extends Sprite
	{
		public var document:XML;
		public var style_list:Array = [{size:12, color:0}];
		public var doc_xpos:Number = 0;
		public var doc_ypos:Number = 0;
		public var doc_width:Number = 500;
		public var doc_height:Number = 500;

		public function GeneralBrowser (_width:Number, _height:Number, font_size:Number=12, font_color:uint=0)
		{
			doc_width = _width;
			doc_height = _height;
			style_list.push ({size:font_size, color:font_color});
			this.cacheAsBitmap = true;
			this.cacheAsBitmapMatrix = new Matrix;
		}
		public function Load (url:String):void
		{
			GeneralLoader.LoadURL (url, OnLoad);
		}
		public function OnLoad (loader:CustomURLLoader):void
		{
			DrawPage (String (loader.data));
		}
		public function Clear() : void
		{
			while (numChildren) removeChildAt (0);
			doc_xpos = 0;
			doc_ypos = 0;
		}
		public function DrawPage (doc_string:String):void
		{
			document = XML (doc_string);
			if (document.head.style.length()){
				var css:String = document.head.style[0];
				GeneralText.SetStyles (css);}
			ParseNode (document.body[0]);
		}
		public function ParseNode (node:XML):void
		{
			var tag:String = node.name();
			var kind:String = node.nodeKind();
			if (kind == "text")
				RenderNode ("", node.toXMLString());

			var child_list:XMLList = node.children();
			for each (var child:XML in child_list){
				var tag:String = child.name();
				if (tag == "p" || tag=="li" || tag=="font" || tag=="h1" || tag=="h2")
					RenderNode (tag, child.toXMLString());
				else
					ParseNode (child);}
		}
		public function RenderNode (tag:String, string:String):void
		{
			var size:Number = Style().size;
			var color:Number = Style().color;
			if (tag == "p" || tag == "h1" || tag == "h2"){ if (doc_ypos > 0) doc_ypos += 4;}
			var text_sprite:CustomSprite = GeneralText.MakeText (string, size, doc_xpos, doc_ypos, color, doc_width);
			doc_ypos += text_sprite.height;
			if (tag == "h1") doc_ypos += 2;
			addChild (text_sprite);
		}
		public function Style ():*
		{
			return style_list[style_list.length-1];
		}
	}
}
