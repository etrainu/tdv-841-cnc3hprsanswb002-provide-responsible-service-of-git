package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.general.*;

	public class QuestionDragDropImageFlow extends QuestionDragDrop
	{
		public function QuestionDragDropImageFlow (ques_xml:XML):void
		{
			super (ques_xml);

			var drop_count:int = 0;
			var option_xml_list:XMLList = ques_xml.option;
			for each (var option_xml:XML in option_xml_list){
				var type:String = option_xml.@type;
				if (type == "drop") drop_count++;}

			var area_width:int = options_width ? options_width : Course.page_width;
			drop_width = (area_width - Option.box_gap * (drop_count - 1)) / drop_count;

			for each (option_xml in option_xml_list){
				type = option_xml.@type;
				if (type == "drag"){
					var option_drag:OptionDragImage = new OptionDragImage (option_xml, DownHandler, DragHandler, DropHandler, OptionDragImage.image_width, OptionDragImage.image_height);
					options_drag.push (option_drag);}
				else if (type == "drop"){
					var option_drop:OptionDropWindow = new OptionDropWindow (option_xml, drop_width);
					options_drop.push (option_drop);}}

			// shuffle the drag boxes
			var count:int = 0;
			var length:int = options_drag.length;
			while (count < length && length > 1){
				var i:int = Math.random() * length;
				var j:int = Math.random() * length;
				if (i != j){
					var temp:* = options_drag[i];
					options_drag[i] = options_drag[j];
					options_drag[j] = temp;
					count++;}}
		}
	}
}
