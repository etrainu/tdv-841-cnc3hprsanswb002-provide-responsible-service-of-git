package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	
	import source.general.*;

	public class Element
	{
		public var element_xml:XML;
		public var element_type:String;
		public var element_sprite:Sprite;
		public var element_width:int = 0;
		public var element_height:int = 0;
		public var element_margin_top:int = 0;
		public var element_margin_bottom:int = 0;
		public var element_margin_left:int = 0;
		public var element_margin_right:int = 0;
		public var element_padding_width:int = 0;
		public var element_padding_height:int = 0;
		public var element_enabled:Boolean = true;
		public var element_slide:CourseSlide;
		public var element_transitions:Array = [];
		public var element_filters:Array = null;
		public var element_events:Array = [];
		// flags
		public var is_text:Boolean = false;
		public var is_image:Boolean = false;
		public var is_graphical:Boolean = false;
		public var is_playable:Boolean = false;
		public var allow_timing:Boolean = false;
		public var allow_filter:Boolean = false;
		public var variable_width:Boolean = false;
		public var fixed_width:Boolean = false;
		public var first_countable:Boolean = false;
		public var last_countable:Boolean = false;
		public var has_label:Boolean = false;
		public var has_played:Boolean = false;

		public function Setup (_xml:XML, _slide:CourseSlide):void
		{
			element_xml = _xml;
			element_type = _xml.@type;
			element_slide = _slide;
		}
		public function AddTo (parent:Sprite):void
		{
			if (!parent.contains (element_sprite)){
				parent.addChild (element_sprite);

				for each (var event:ElementEvent in element_events){
					event.AddMouseListener (this);}

				for each (var transition:ElementTransitionEffect in element_transitions){
					transition.Apply (this);}

				if (element_filters)
					element_sprite.filters = element_filters;}
		}
		public function SetMediaEvents ():void
		{
			for each (var event:ElementEvent in element_events){
				event.AddMediaListener (this);}
		}
		public function RemoveFrom (parent:Sprite):void
		{
			if (element_sprite){
				if (parent.contains (element_sprite)){
					parent.removeChild (element_sprite);}}
		}
		public function EventHandler (event:Event = null):void
		{
			var label:String = xml.@label;
			var action:String = xml.@action;
			if (action == "show") slide.ShowLabel (label, true, true);
			else if (action == "show all") slide.ShowAll (true, true);
			else if (action == "show only"){ slide.ShowAll (false, false); slide.ShowLabel (label, true, true);}
			else if (action == "hide") slide.ShowLabel (label, false, true);
			else if (action == "hide all") slide.ShowAll (false, true);
			else if (action == "load course") Course.LoadCourseID (label);
			else if (action == "stop sound") GeneralSound.Stop();
			else if (action == "email to") navigateToURL (new URLRequest ("mailto:" + label));
			else if (action == "link to") navigateToURL (new URLRequest (label), "_blank");
		}
		public function SetEnabled (state:Boolean):void
		{
			if (!state && element_enabled) has_played = false;
			element_enabled = state;
		}

		public function get xml ():XML { return element_xml; }
		public function get type ():String { return element_type; }
		public function get label ():String { return element_xml.@label; }
		public function get width ():int { return element_width; }
		public function get height ():int { return element_height; }
		public function get sprite ():Sprite { return element_sprite; }
		public function get slide ():CourseSlide { return element_slide; }
		public function get enabled ():Boolean { return element_enabled; }
		public function get events ():Array { return element_events; }
		public function get filters ():Array { return element_filters; }
		public function get transitions ():Array { return element_transitions; }
		public function get can_enable ():Boolean { return is_graphical || is_playable; }
		public function get x ():int { return sprite.x;}
		public function get y ():int { return sprite.y;}
		public function set x (_x:int):void { sprite.x = _x;}
		public function set y (_y:int):void { sprite.y = _y;}

		public function ProperType ():String { return element_type.substr(0,1).toUpperCase() + element_type.substr(1);}
		public function PrepareElement (array:Array, index:int):void {}
		public function PrepareGroup (array:Array, index:int):void {}
		public function PrepareGroupElement (array:Array, index:int):void {}
		public function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean { return false;}
		public function GetMinimumWidth ():int { return fixed_width ? element_width : 0;}
		public function IsVariableWidth():Boolean { return variable_width;}
		public function GroupEnable (state:Boolean):void { element_enabled = state;}
		public function ResolveText ():String { return GeneralString.Resolve (element_xml.text); }
		public function StopShowing ():Boolean { return false; }
		public function PlayElement ():void {}
		public function EnterFrame ():void {}
		public function UnloadAndStop ():void {}

		public function DocumentElement ():String { return "";}
		public function DocumentComment (out:String):String { return "<p class=\"comment\">" + out + "</p>";}
		public function DocumentTree (group_array:Array):String
		{
			var out:String = "";
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				out += element.DocumentElement ();}
			return out;
		}
		public function DocumentBlock (group_array:Array):String
		{
			var out:String = "<table><tr><td class=\"border\">";
			out += DocumentComment ("[ " + ProperType() + " " + label + " ]");
			out += DocumentTree (group_array);
			out += "</td></tr></table>";
			return out;
		}
	}
}
