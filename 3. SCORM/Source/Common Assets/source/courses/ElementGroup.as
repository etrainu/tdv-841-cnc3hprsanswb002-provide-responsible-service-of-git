package source.courses
{
	import flash.display.*;
	import source.general.*;
	import flash.events.*;
	import flash.utils.*;

	public class ElementGroup extends Element
	{
		public var element_array:Array = [];

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
			has_label = true;
		}
		public override function PrepareGroupElement (array:Array, index:int):void
		{
			var count_down:int = xml.@count;
			while (count_down > 0 && index < array.length){
				var element:Element = array[index++];
				if (element.can_enable) count_down--;
				element_array.push (element);}
			GroupEnable (String (xml.@enabled) == "true");
		}
		public override function GroupEnable (state:Boolean):void
		{
			for each (var element:Element in element_array){
				element.SetEnabled (state);}
		}
		public override function DocumentElement ():String
		{
			return DocumentComment ("[ Group " + String (xml.@label) + "]");
		}
	}
}
