package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.custom.*;
	import source.general.*;

	public class OptionDropWindowFixed extends OptionDropWindow
	{
		public function OptionDropWindowFixed (xml:XML, _width:int)
		{
			super (xml, _width);
			fixed_height = 140;
			stacking_factor = 5;
			drop_scale = (fixed_height - label_height - Option.box_gap*2) / OptionDragImage.image_height;
		}
	}
}
