package source.courses
{
	import flash.utils.*;
	
	import source.custom.*;
	import source.general.*;

	public class CourseStyles
	{
		public static function LoadURL (url:String):void
		{
			if (url && url.length){
				GeneralLoader.LoadURL (Course.ResolveMediaPath ("styles", url), LoadComplete);}
		}
		public static function LoadComplete (loader:CustomURLLoader):void
		{
			SetStyles (String(loader.data));
		}
		public static function SetStyles (styles:String):void
		{
			var line_array:Array = styles.split ("\n");
			for each (var line:String in line_array){
				var clean_line:String = line.replace (/[\x00-\x20;]/g, "").split("/")[0];
				if (!clean_line.length) continue;
				var assign_array:Array = clean_line.split ("=");
				var dot_array:Array = assign_array[0].split (".");
				var class_name:String = dot_array[0];
				var style_name:String = dot_array[1];
				var value_name:String = assign_array[1];
				SetStyle (class_name, style_name, value_name);}
		}
		public static function SetStyle (class_name:String, style_name:String, value_string:String):void
		{
			var prefix:String = (class_name.slice(0,7) == "General") ? "source.general." : "source.courses." 
			var source_class:Class = getDefinitionByName (prefix + class_name) as Class;
			var old_value:* = source_class[style_name];
			source_class[style_name] = ConvertValue (value_string);
		}
		public static function ConvertValue (value_string:String):*
		{
			var value:* = "";
			if (value_string == "true") value = true;
			else if (value_string == "false") value = false;
			else if (value_string.charAt(0) == "\"") value = value_string.slice (1,-1);
			else if (value_string.slice(0,2) == "0x") value = uint (value_string)
			else if (value_string.slice(0,2) == "#") value = uint ("0x" + value_string.slice(1));
			else if (value_string.charAt(0).search (/[0-9]/) != -1) return Number (value_string);
			else if (value_string.charAt(0) == "["){
				value = [];
				var array:Array = value_string.slice(1,-1).split(",");
				for (var i:int=0; i<array.length; i++){
					value.push (ConvertValue (array[i]));}}
			return value;		
		}
		public static function SetTextColor (color:uint):void
		{
			SetTextColorCore (ElementParagraph.text_color_array, color);
			SetTextColorCore (ElementHeading.text_color_array, color);
			SetTextColorCore (ElementListItem.text_color_array, color);
			// Assessments too
			Question.title_color = color;
			Question.text_color = color;
			Question.help_color = color;
			Option.text_color = color;
			Question.result_color = color;
			Assessment.message_color = color;
		}
		public static function SetTextColorCore (array:Array, color:uint):void
		{
			for  (var i:int=0; i<array.length; i++){
				array[i] = color;}
		}
		public static function SetTextSize (size:int):void
		{
			var scale:Number = Number (size) / 16;
			ScaleTextSizeCore (ElementParagraph.text_size_array, scale);
			ScaleTextSizeCore (ElementHeading.text_size_array, scale);
			ScaleTextSizeCore (ElementListItem.text_size_array, scale);
		}
		public static function ScaleTextSizeCore (array:Array, scale:Number):void
		{
			for  (var i:int=0; i<array.length; i++){
				array[i] = int (array[i] * scale);}
		}
	}
}
