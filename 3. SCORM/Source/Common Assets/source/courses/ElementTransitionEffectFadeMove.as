package source.courses
{
	import flash.events.*;
	import flash.display.*;

	public class ElementTransitionEffectFadeMove extends ElementTransitionEffectFade
	{
		public static var move_factor:Number = 1.5;
		public static var move_distance:Number = 50;

		public var offset_x:int = 0;
		public var offset_y:int = 0;
		public var desired_x:int;
		public var desired_y:int;

		public function ElementTransitionEffectFadeMove (dx:int, dy:int)
		{
			offset_x = dx * move_distance;
			offset_y = dy * move_distance;
		}
		public override function Apply (element:Element):Boolean
		{
			if (super.Apply (element)){
				sprite.addEventListener (Event.ENTER_FRAME, EnterFrame);
				desired_x = sprite.x;
				desired_y = sprite.y;
				sprite.x += offset_x;
				sprite.y += offset_y;
				sprite.alpha = 0;
				return true;}
			return false;
		}
		public override function EnterFrame (event:Event):void
		{
			var alpha:Number = sprite.alpha;
			alpha = Math.min (1, alpha + alpha_factor);
			sprite.alpha = alpha;

			var delta_x:Number = desired_x - sprite.x;
			if (Math.abs (delta_x) > 1) sprite.x += delta_x / move_factor;
			else { sprite.x = desired_x; delta_x = 0;}

			var delta_y:Number = desired_y - sprite.y;
			if (Math.abs (delta_y) > 1) sprite.y += delta_y / move_factor;
			else { sprite.y = desired_y; delta_y = 0;}

			if (alpha == 1 && delta_x == 0 && delta_y == 0){
				sprite.removeEventListener (Event.ENTER_FRAME, EnterFrame);}
		}
	}
}
