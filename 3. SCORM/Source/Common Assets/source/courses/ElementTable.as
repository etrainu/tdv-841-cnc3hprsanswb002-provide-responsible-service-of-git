package source.courses
{
	import flash.display.*;
	import source.general.*;
	import source.courses.*;

	public class ElementTable extends Element
	{
		public var group_array:Array;
		public var layout_array:Array;
		public var layout:Object;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
			is_graphical = true;
		}
		public override function PrepareGroup (array:Array, index:int):void
		{
			index++;
			var count_down:int = element_xml.@count;

			for (var i:int = index; i<array.length && count_down > 0; i++){
				var element:Element = array[i];
				if (element.can_enable){
					count_down--;}}

			group_array = array.splice (index, i - index);

			FindGraphicalEnds();
		}
		public override function PrepareGroupElement (array:Array, index:int):void
		{
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				element.PrepareGroupElement (group_array, i);}
		}
		public function FindGraphicalEnds ():void
		{
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				if (element.is_graphical){
					element.first_countable = true;
					break;}}

			for (i=group_array.length-1; i>=0; i--){
				element = group_array[i];
				if (element.is_graphical){
					element.last_countable = true;
					break;}}
		}
		public function GetVariableWidthCount ():int
		{
			var count:int = 0;
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				if (element.IsVariableWidth()){
					count++;}}
			return count;
		}
		public override function IsVariableWidth():Boolean
		{
			return GetVariableWidthCount() > 0;
		}
		public function AlignElements (layout_width:int, layout_height:int):void
		{
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];

				if (element.type == "align"){
					var old_align:String = layout.align;
					PushLayout (element);
					if (layout.align == "indent"){
						layout.bound_left += int (GeneralConversions.ScaleToValueOrPercent (element.xml.@width, layout_width));
						layout.align = old_align;}
					else if (layout.align == "offset"){
						layout.offset_x = int (GeneralConversions.ScaleToValueOrPercent (element.xml.@width, layout_width));
						layout.offset_y = int (GeneralConversions.ScaleToValueOrPercent (element.xml.@height, layout_height));
						layout.align = old_align;}}

				else if (element.sprite){
					if (IsLayout ("right")){
						element.x = layout.bound_right - element.width;}
					if (IsLayout ("center")){
						element.x = layout.bound_left + (layout.bound_right - layout.bound_left) / 2 - element.width / 2;}
					if (IsLayout ("left")){
						element.x = layout.bound_left;}
					if (IsLayout ("bottom")){
						element.y = layout.bound_bottom - element.height;}
					if (IsLayout ("middle")){
						element.y = (layout.bound_bottom - element_padding_height) / 2 - element.height / 2;}
					if (IsLayout ("top")){
						element.y = layout.bound_top;}
					element.x += layout.offset_x;
					element.y += layout.offset_y;
					PopLayout ();}}
		}
		public function IsLayout (align:String):Boolean
		{
			return layout.align.indexOf (align) != -1;
		}
		public function AbsoluteLayout ():Boolean
		{
			return layout.align.split(" ").length == 2;
		}
		public function PushLayout (element:Element):void
		{
			layout_array.push (GeneralConversions.CopyObject(layout));
			layout.align = String (element.xml.@align);
			layout.count = int (element.xml.@count);
			if (!layout.count) layout.count = -1;
		}
		public function PopLayout ():void
		{
			if (layout.count){
				layout.count--;
				if (layout.count == 0){
					layout = layout_array.pop();
					PopLayout ();}}
		}
		public override function DocumentElement ():String
		{
			return DocumentTree (group_array);
		}
	}
}
