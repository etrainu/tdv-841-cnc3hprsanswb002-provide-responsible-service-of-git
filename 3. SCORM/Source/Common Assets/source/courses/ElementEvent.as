package source.courses
{
	import flash.display.*;
	import source.general.*;
	import flash.events.*;
	import flash.utils.*;

	public class ElementEvent extends Element
	{
		public var no_repeat_array:Array = [];

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
		}
		public override function PrepareGroupElement (array:Array, index:int):void
		{
			var count_down:int = xml.@count;
			while (count_down > 0 && index < array.length){
				var element:Element = array[index++];
				if (element.can_enable){
					element.events.push (this);
					count_down--;}}
		}
		public function AddMouseListener (element:Element):void
		{
			var mouse_event:String;
			var event:String = xml.@event;
			if (event == "click") mouse_event = MouseEvent.CLICK;
			else if (event == "mouse over") mouse_event = MouseEvent.MOUSE_OVER;
			else if (event == "mouse out") mouse_event = MouseEvent.MOUSE_OUT;

			if (no_repeat_array.indexOf (element.sprite) == -1){
				if (mouse_event && element.sprite){
					element.sprite.addEventListener (mouse_event, EventHandler);
					element.sprite.buttonMode = true;
					element.sprite.mouseChildren = false;
					no_repeat_array.push (element.sprite);}}
		}
		public function AddMediaListener (element:Element):void
		{
			var event:String = xml.@event;

			if (element.type == "sound" && event == "finish"){
				Object (element).function_finish = EventHandler;}

			if (element.type == "video" && event == "finish"){
				Object (element).function_finish = EventHandler;}
		}
		public override function DocumentElement ():String
		{
			return DocumentComment ("[ Event: on " + String (xml.@event) + " .. " + String (xml.@action) + " \"" + String (xml.@label) + "\" ]");
		}
	}
}
