package source.courses
{
	import flash.display.*;
	import source.general.*;
	import flash.events.*;
	import flash.utils.*;

	public class ElementEventTimer extends Element
	{
		public var timer:Timer;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (has_played) return false;
			SetEnabled (false);
			var delay:Number = Number (xml.@delay);
			timer = new Timer (delay * 1000, 1);
			timer.addEventListener (TimerEvent.TIMER, TimerHandler);
			timer.start();
			has_played = true;
			return false;
		}
		public function TimerHandler (event:TimerEvent):void
		{
			EventHandler ();
			slide.LayoutAndDraw();
		}
		public override function DocumentElement ():String
		{
			return DocumentComment ("[ Time event after " + String (xml.@delay) + " .. " + String (xml.@action) + " \"" + String (xml.@label) + "\" ]");
		}
		public override function UnloadAndStop ():void
		{
			if (timer) timer.stop();
		}
	}
}
