package source.courses
{
	import flash.display.*;
	import source.general.*;
	import source.courses.*;

	public class ElementTableRow extends ElementTable
	{
		public var column_width_array:Array = [];
		private var total_gap_width:int = 0;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
		}
		public override function PrepareElement (array:Array, index:int):void
		{
			var count:int = xml.@count;
			var widths:String = xml.@widths;
			var split_array:Array = widths.split(',');
			if (split_array.length){
				for (var i:int=0; i<count; i++){
					if (i < split_array.length){
						var trim:String = GeneralString.Trim (split_array[i]);
						if (trim.length){
							column_width_array.push (trim);}}}}
		}
		public override function GetMinimumWidth ():int
		{
			var total_width:int = 0;
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				var min_width:int = element.GetMinimumWidth();
				total_width += min_width;}

			return total_width;
		}
		public function GetVariableColumnWidth (layout_width:int):int
		{
			// This function subtracts all fixed column widths and divides the result by the number of variable width columns
			// The result is used to set the width of variable width columns containing text.
			// Not quite as good as a HTML browser layout engine, but it does the job quite nicely.
			// In problem cases, override the auto-width calculation using the row element 'widths' parameter
			
			var gap:int = xml.@gap;
			var total_fixed_width:int = 0;
			var minimum_fixed_width:int = 0;
			var total_margin_width:int = 0;
			var variable_element_count:int = 0;
			var is_graphical_array:Array = [];

			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				if (element.is_graphical){
					var min_width:int = element.GetMinimumWidth();
					var index:int = is_graphical_array.length;
					if (!min_width && index < column_width_array.length){
						min_width = GeneralConversions.ScaleToValueOrPercent (column_width_array[index], layout_width);}
					if (!min_width) variable_element_count++;
					total_fixed_width += min_width;
					is_graphical_array.push (element);}}

			for (i=0; i<is_graphical_array.length - 1; i++){
				var element1:Element = is_graphical_array[i];
				var element2:Element = is_graphical_array[i+1];
				var width:int = Math.max (element1.element_margin_right, element2.element_margin_left);
				total_margin_width += width;}

			total_gap_width = (is_graphical_array.length - 1) * gap;
			return (layout_width - total_fixed_width - total_margin_width - total_gap_width) / variable_element_count;
		}
		public function DefaultAlign ():String { return "top";}

		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			var gap:int = xml.@gap;
			var cursor_x:int = element_padding_width;
			var margin_x:int = 0;

			element_width = 0;
			if (fix_width) element_width = layout_width - element_padding_width*2;
			element_height = 0;
			if (!element_sprite)
				element_sprite = new Sprite;

			layout_array = [];
			layout = {
				count: 0,
				align: DefaultAlign(),
				gap: gap };

			var variable_column_width:int = GetVariableColumnWidth (layout_width);

			var graphical_element_count:int = 0;
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				if (!element.enabled){
					element.RemoveFrom (element_sprite);
					continue;}
				element.SetMediaEvents();

				var minimum_width:int = element.GetMinimumWidth();
				var column_width:int = Math.max (variable_column_width, minimum_width);
				var column_width_fixed:Boolean =  false;
				if (graphical_element_count < column_width_array.length){
					column_width = GeneralConversions.ScaleToValueOrPercent (column_width_array[graphical_element_count], layout_width - total_gap_width);
					column_width_fixed = true;}

				if (element.type == "align"){
					PushLayout (element);
					if (layout.align == "gap"){
						layout.gap = element.xml.@width;}}

				else if (element.DoLayout (column_width, 0, column_width_fixed)){
					if (!AbsoluteLayout()){

						// top margin
						element_margin_top = Math.max (element_margin_top, element.element_margin_top);
	
						// left margin
						if (element.first_countable) element_margin_left = Math.max (element_margin_left, element.element_margin_left);
						else margin_x = Math.max (margin_x, element.element_margin_left, layout.gap);
						cursor_x = cursor_x + margin_x;
						margin_x = 0;

						// position
						element.x = cursor_x;
						var best_width:int = column_width_fixed ? Math.max (column_width, element.width) : element.width
						cursor_x += best_width;

						// size
						element_width = Math.max (element_width, cursor_x);
						element_height = Math.max (element_height, element.height + element_padding_height*2);

						// right margin
						if (element.last_countable) element_margin_right = Math.max (element_margin_right, element.element_margin_right);
						else margin_x = Math.max (margin_x, element.element_margin_right, layout.gap);

						// bottom
						element_margin_bottom = Math.max (element_margin_bottom, element.element_margin_bottom);}

					element.AddTo (element_sprite);
					graphical_element_count++;
					PopLayout ();}}

			layout_height = Math.max (layout_height, element_height);
			layout = {
				count: 0,
				align: DefaultAlign(),
				bound_top: element_padding_height,
				bound_left: element_padding_width,
				bound_right: element_width,
				bound_bottom: layout_height,
				offset_x: 0,
				offset_y: 0 };

			AlignElements (layout_width, layout_height);

			element_width += element_padding_width;

			return graphical_element_count > 0;
		}
	}
}
