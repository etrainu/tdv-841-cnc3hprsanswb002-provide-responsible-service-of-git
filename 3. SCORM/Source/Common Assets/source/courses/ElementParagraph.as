package source.courses
{
	import flash.display.*;
	import source.general.*;
	import source.custom.*;

	public class ElementParagraph extends Element
	{
		public static var style_array:Array = ["large","medium","bold","italic","bold italic","centered","small","tiny"];
		public static var text_size_array:Array = [20,16,16,16,16,16,12,9];
		public static var text_color_array:Array = [0x505050,0x000000,0x505050,0x000000,0x505050,0x000000,0x000000,0x505050];
		public static var margin_top_array:Array = [6,6,6,6,6,6,4,2];
		public static var margin_bottom_array:Array = [10,8,8,8,8,8,6,4];
		public var style_index:int = 1;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			var style:String = _xml.@style;
			style_index = style_array.indexOf (style);
			if (style_index == -1) style_index = 1; // default medium

			element_margin_top = margin_top_array[style_index];
			element_margin_bottom = margin_bottom_array[style_index];

			is_text = true;
			variable_width = true;
			is_graphical = true;
			allow_timing = true;
			allow_filter = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (element_sprite){
				GeneralText.ResizeText (CustomSprite(element_sprite), layout_width);}
			else {
				var para_text:String = ResolveText();
				var style:String = String(xml.@style);
				element_sprite = GeneralText.MakeText (para_text, text_size_array[style_index], text_color_array[style_index], 0, 0, fix_width ? layout_width : -layout_width, null, style);}

			element_width = element_sprite.width;
			element_height = element_sprite.height;
			return true;
		}
		public override function DocumentElement ():String
		{
			var text:String = xml.text;
			return "<p>" + text + "</p>";
		}
	}
}
