package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.custom.*;
	import source.general.*;

	public class OptionDrag extends Option
	{
		public var box_width:int;
		public var box_height:int;
		public var desired_x:int;
		public var desired_y:int;
		public var desired_scale:Number = 1;
		public var dragging:Boolean = false;
		public var dropping:Boolean = false;
		public var down_handler:Function;
		public var move_handler:Function;
		public var drop_handler:Function;
		public var drop_window:OptionDropWindow;

		public function OptionDrag (xml:XML, _width:int, _height:int, down:Function, move:Function, drop:Function)
		{
			super (xml);
			box_width = _width;
			box_height = _height;
			down_handler = down;
			move_handler = move;
			drop_handler = drop;
		}
		public function ShowAt (_x:int, _y:int):void
		{
			desired_x = x = _x;
			desired_y = y = _y;
		}
		public function OnMouseDown (event:MouseEvent):void
		{
			stage.addEventListener (MouseEvent.MOUSE_UP, OnMouseUp);
			stage.addEventListener (MouseEvent.MOUSE_MOVE, OnMouseMove);
			this.startDrag ();
			dragging = true;
			if (down_handler != null)
				down_handler (this);
		}
		public function OnMouseUp (event:MouseEvent):void
		{
			this.stopDrag ();
			stage.removeEventListener (MouseEvent.MOUSE_UP, OnMouseUp);
			dragging = false;
			dropping = true;
			if (drop_handler != null)
				drop_handler (this);
		}
		public function OnMouseMove (event:MouseEvent):void
		{
			if (dragging){
				if (move_handler != null){
					move_handler (this);}}
		}
		public function OnEnterFrame (event:Event):void
		{
			if (!dragging){
				// position
				x += (desired_x - x) / tween_factor;
				y += (desired_y - y) / tween_factor;
				if (Math.abs (desired_x - x) < 1 && Math.abs (desired_y - y) < 1){
					x = desired_x;
					y = desired_y;}
				// scale
				scaleX = scaleY = scaleX + (desired_scale - scaleX) / tween_factor;
				if (Math.abs (desired_scale - scaleX) < 0.01){
					scaleX = scaleY = desired_scale;}
			}
		}
	}
}
