package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.general.*;

	public class QuestionDragDropFixed extends QuestionDragDrop
	{
		public var drag_bases:Sprite = new Sprite;

		public function QuestionDragDropFixed (xml:XML):void
		{
			super (xml);
		}
		public override function RenderQuestion ():Sprite
		{
			question_sprite = new Sprite;
			var head_sprite:Sprite = GetQuestionHead();
			question_sprite.addChild (head_sprite);

			drop_xpos = 0;
			drag_ypos = drop_ypos = head_sprite.height + text_gap;
			if (options_xpos != 0) drag_xpos = drop_xpos = options_xpos;
			if (options_ypos != 0) drag_ypos = drop_ypos = options_ypos;

			drag_xpos = Option.box_gap;
			var drag_bases:Sprite = new Sprite;
			question_sprite.addChild (drag_bases);
			LayoutDragBoxes (question_sprite);

			LayoutDropWindows (question_sprite);

			return question_sprite;
		}
		public override function LayoutDropWindows (question_sprite:Sprite=null):void
		{
			var xpos:int = drop_xpos;
			var max_height:int = 0;
			for each (var droption:OptionDropWindow in options_drop){
				droption.x = xpos;
				droption.y = drop_ypos;
				droption.Render ();
				xpos += drop_width + Option.box_gap;
				if (question_sprite){
					question_sprite.addChild (droption);}
				max_height = Math.max (max_height, droption.height);}
		}
		public override function LayoutDragBoxes (question_sprite:Sprite=null):void
		{
			var max_height:int = 0;
			var xpos:int = drag_xpos;
			var ypos:int = drag_ypos;
			var area_width:int = options_width ? options_width : Course.page_width;

			for each (var dragtion:OptionDrag in options_drag){
				if (!dragtion.drop_window){
					if (!dragtion.dragging){
						if (xpos + dragtion.box_width > drag_xpos + area_width){
							xpos = drag_xpos;
							ypos += dragtion.box_height + Option.box_gap;}
						dragtion.ShowAt (xpos, ypos);
						xpos += dragtion.box_width + Option.box_gap;
						max_height = Math.max (max_height, dragtion.height);}}}

			if (question_sprite){
				for each (dragtion in options_drag){
					question_sprite.addChild (dragtion);}}

			drop_ypos = ypos += dragtion.box_height + Option.box_gap * 2;
		}
	}
}
