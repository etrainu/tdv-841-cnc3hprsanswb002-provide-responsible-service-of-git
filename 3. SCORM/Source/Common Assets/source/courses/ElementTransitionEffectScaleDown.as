package source.courses
{
	import flash.events.*;
	import flash.display.*;

	public class ElementTransitionEffectScaleDown extends ElementTransitionEffectFade
	{
		public static var scale_factor:Number = 4;

		public override function Apply (element:Element):Boolean
		{
			if (super.Apply (element)){
				sprite.addEventListener (Event.ENTER_FRAME, EnterFrame);
				sprite.scaleX = sprite.scaleY = scale_factor;
				sprite.alpha = 0;
				return true;}
			return false;
		}
		public override function EnterFrame (event:Event):void
		{
			var alpha:Number = sprite.alpha;
			alpha = Math.min (1, alpha + alpha_factor);
			sprite.alpha = alpha;

			var scale:Number = sprite.scaleX;
			scale = Math.max (1, scale - alpha_factor * scale_factor);
			sprite.scaleX = sprite.scaleY = scale;

			if (alpha == 1 && scale == 1){
				sprite.removeEventListener (Event.ENTER_FRAME, EnterFrame);}
		}
	}
}
