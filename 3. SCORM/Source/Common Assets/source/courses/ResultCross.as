package source.courses
{
	import flash.display.*;

	public class ResultCross extends Sprite
	{
		public static var color:uint = 0x900000;
		
		public function ResultCross()
		{
			var s:Sprite = new Sprite;
			var g:Graphics = s.graphics;
			g.lineStyle (3, color, 1);
			g.moveTo (4,32);
			g.curveTo (18,12, 28,5);
			g.moveTo (4,32);
			g.curveTo (18,16, 34,8);
			g.moveTo (20,12);
			g.lineTo (30,6);
			g.lineStyle (2, color);
			g.moveTo (20,12);
			g.lineTo (2,35);
			g.lineStyle (3, color);
			g.moveTo (8,8);
			g.curveTo (20,12, 30,24);
			g.moveTo (6,10);
			g.curveTo (16,14, 30,30);
			g.moveTo (18,16);
			g.lineTo (30,27);
			s.scaleX = s.scaleY = 0.41;
			s.y = 2;
			addChild (s);
		}
	}
}
