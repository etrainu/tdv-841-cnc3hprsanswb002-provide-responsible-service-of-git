package source.courses
{
	import flash.display.*;
	import flash.filters.*;
	import flash.events.*;
	import flash.geom.*;
	import source.general.*;

	public class ElementButton extends ElementTableRow
	{
		public static var margin_top:int = 10;
		public static var margin_bottom:int = 10;
		public static var margin_left:int = 10;
		public static var margin_right:int = 10;
		public static var padding_width:int = 12;
		public static var padding_height:int = 4;
		public static var row_gap:int = 6;
		public static var rounding:int = 12;

		public var border_sprite:Sprite;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			element_margin_top = margin_top;
			element_margin_bottom = margin_bottom;
			element_margin_left = margin_left;
			element_margin_right = margin_right;
			element_padding_width = padding_width;
			element_padding_height = padding_height;
			xml.@gap = row_gap;
		}

		public override function DefaultAlign ():String { return "middle";}

		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			super.DoLayout (layout_width, layout_height);
			
			if (fix_width) element_width = layout_width;

			var matrix:Matrix = new Matrix;
			var graphics:Graphics = element_sprite.graphics;
			matrix.createGradientBox (element_width, element_height, Math.PI/2);
			graphics.clear();
			graphics.beginGradientFill ("linear", [0xFFFFFF,0xF6F6F6,0xF0F0F0,0xE4E4E4], [1,1,1,1], [0,55,200,255], matrix);
			graphics.drawRoundRect (0, 0, element_width, element_height, rounding, rounding);
			graphics.endFill();
			element_sprite.filters = [new DropShadowFilter (0.6,45,0,0.9,2,2,1,4)];

			if (!element_sprite.buttonMode){
				element_sprite.addEventListener (MouseEvent.CLICK, EventHandler);
				element_sprite.buttonMode = true;
				element_sprite.mouseChildren = false;}

			return true;
		}
		public override function DocumentElement ():String
		{
			return DocumentBlock (group_array);
		}
	}
}
