package source.courses
{
	import flash.display.*;
	import source.general.*;
	import flash.events.*;
	import flash.utils.*;

	public class ElementTransition extends Element
	{
		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
		}
		public override function PrepareElement (array:Array, index:int):void
		{
			var count_down:int = xml.@count;
			if (!count_down) count_down = -1;
			while (count_down && index < array.length){
				var element:Element = array[index++];
				if (element.is_graphical){
					var effect:ElementTransitionEffect = GetEffect();
					if (effect) element.transitions.push (effect);
					count_down--;}}
		}
		public function GetEffect ():ElementTransitionEffect
		{
			var name:String = element_xml.@effect;
			var effect:ElementTransitionEffect;
			if (name == "fade") effect = new ElementTransitionEffectFade;
			else if (name == "fade right"){ effect = new ElementTransitionEffectFadeMove (-1, 0); }
			else if (name == "fade left"){ effect = new ElementTransitionEffectFadeMove (1, 0); }
			else if (name == "fade down"){ effect = new ElementTransitionEffectFadeMove (0, -1); }
			else if (name == "fade up"){ effect = new ElementTransitionEffectFadeMove (0, 1); }
			else if (name == "scale down") effect = new ElementTransitionEffectScaleDown;
			return effect;
		}
	}
}
