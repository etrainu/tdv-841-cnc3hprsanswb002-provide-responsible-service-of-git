package source.courses
{
	import flash.display.*;
	import source.general.*;
	import source.courses.*;

	public class ElementTableColumn extends ElementTable
	{
		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
		}
		public override function GetMinimumWidth ():int
		{
			var max_width:int = 0;
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				if (element.variable_width) variable_width = true;
				var min_width:int = element.GetMinimumWidth();
				max_width = Math.max (min_width, max_width);}

			if (!variable_width){
				max_width += element_padding_width * 2
				element_width = max_width;
				fixed_width = true;}

			return max_width;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			var gap:int = xml ? xml.@gap : 0;
			var cursor_y:int = element_padding_height;
			var margin_y:int = 0;

			element_width = 0;
			element_height = 0;
			if (!element_sprite)
				element_sprite = new Sprite;

			layout_array = [];
			layout = {
				count: 0,
				align: "left",
				bound_left: element_padding_width,
				bound_right: (layout_width - element_padding_width),
				fix_width: false,
				gap: gap };

			var graphical_element_count:int = 0;
			for (var i:int=0; i<group_array.length; i++){
				var element:Element = group_array[i];
				if (!element.enabled){
					element.RemoveFrom (element_sprite);
					continue;}
				element.SetMediaEvents();

				if (element.type == "align"){
					PushLayout (element);
					if (layout.align == "width"){
						var new_layout_width:int = GeneralConversions.ScaleToValueOrPercent (element.xml.@width, layout_width);
						layout.bound_right = layout.bound_left + new_layout_width - element_padding_width;
						layout.fix_width = true;}
					else if (layout.align == "indent"){
						layout.bound_left += int (GeneralConversions.ScaleToValueOrPercent (element.xml.@width, layout_width));}
					else if (layout.align == "gap"){
						layout.gap = element.xml.@width;}}

				else if (element.DoLayout (layout.bound_right - layout.bound_left, 0, layout.fix_width)){
					if (!AbsoluteLayout()){

						// top margin
						if (element.first_countable) element_margin_top = Math.max (element_margin_top, element.element_margin_top);
						else margin_y = Math.max (margin_y, element.element_margin_top, layout.gap);
						cursor_y = cursor_y + margin_y;
						margin_y = 0;

						// position
						element.x = layout.bound_left;
						element.y = cursor_y;
						cursor_y += element.height;

						// size
						element_width = Math.max (element_width, element.width + element_padding_width*2);
						element_height = Math.max (element_height, cursor_y);
						//layout.bound_right = Math.max (layout.bound_right, element_width - element_padding_width); // hopefully kill this

						// bottom margin
						if (element.last_countable) element_margin_bottom = Math.max (element_margin_bottom, element.element_margin_bottom);
						else margin_y = Math.max (margin_y, element.element_margin_bottom, layout.gap);}

					element.AddTo (element_sprite);
					graphical_element_count++;
					PopLayout ();}}

			if (fix_width) element_width = layout_width;

			layout_height = Math.max (layout_height, element_height);
			layout = {
				count: 0,
				align: "left",
				bound_top: element_padding_height,
				bound_left: element_padding_width,
				bound_right: element_width - element_padding_width,
				bound_bottom: layout_height,
				offset_x: 0,
				offset_y: 0 };

			AlignElements (layout_width, layout_height);

			element_height += element_padding_height;

			return graphical_element_count > 0;
		}
	}
}
