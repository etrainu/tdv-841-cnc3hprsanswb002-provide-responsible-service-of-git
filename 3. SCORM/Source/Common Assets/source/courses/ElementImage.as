package source.courses
{
	import flash.display.*;
	import source.general.*;

	public class ElementImage extends Element
	{
		public static var margin_top:int = 0;
		public static var margin_bottom:int = 0;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			var scale:Number = Number (xml.@scale) / 100;
			element_width = Number (xml.@width) * scale;
			element_height = Number (xml.@height) * scale;
			element_margin_top = margin_top;
			element_margin_bottom = margin_bottom;

			is_image = true;
			fixed_width = true;
			is_graphical = true;
			allow_timing = true;
			allow_filter = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (element_sprite) return true;
			element_sprite = new Sprite;
			var image_url:String = Course.ResolveMediaPath ("images", xml.@url);
			var loader:Loader = GeneralLoader.LoadSizedImage (image_url, 0, 0, element_width, element_height);
			element_sprite.addChild (loader);
			return true;
		}
		public override function DocumentElement ():String
		{
			var image_url:String = Course.ResolveMediaPath ("images", xml.@url);
			var image_scale:Number = Number (xml.@scale) / 100;
			var image_width:int = Number (xml.@width) * image_scale;
			var image_height:int = Number (xml.@height) * image_scale;
			return "<img src=\"" + image_url + "\" width=\"" + image_width + "\" height=\"" + image_height + "\" />";
		}
	}
}
