package source.courses
{
	import flash.events.*;
	import flash.display.*;

	public class ElementTransitionEffect extends EventDispatcher
	{
		public var element:Element;
		public var sprite:Sprite;

		public function Apply (_element:Element):Boolean
		{
			element = _element;
			if (_element.sprite){
				sprite = _element.sprite;
				return true;}
			return false;
		}
	}
}
