package source.courses
{
	import flash.display.*;
	import source.general.*;
	import source.custom.*;

	public class ElementListItem extends Element
	{
		public static var text_size_array:Array = [16];
		public static var text_color_array:Array = [0x000000];
		public static var bullet_indent:int = 20;
		public static var bullet_counter:int = 0;
		public static var margin_top:int = 6;
		public static var margin_bottom:int = 6;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			element_margin_top = margin_top;
			element_margin_bottom = margin_bottom;

			is_text = true;
			variable_width = true;
			is_graphical = true;
			allow_timing = true;
			allow_filter = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			var text_width:int = layout_width - bullet_indent;
			
			if (element_sprite){
				GeneralText.ResizeText (CustomSprite(Object(element_sprite).text), text_width);
				element_width = element_sprite.width;
				element_height = element_sprite.height;
				return true;}
			
			var text_size:int = text_size_array[0];
			var text_color:uint = text_color_array[0];

			element_sprite = new CustomSprite;
			var item_text:String = ResolveText();
			var variable_width:int = fix_width ? text_width : -text_width;
			var item_text_sprite:Sprite = GeneralText.MakeText (item_text, text_size, text_color, bullet_indent, 0, variable_width);
			Object(element_sprite).text = item_text_sprite;
			element_sprite.addChild (item_text_sprite);

			var bullet_type:String = String (xml.@bullet);

			if (bullet_type == "circle"){
				var bullet_sprite:Sprite = new Sprite;
				bullet_sprite.graphics.beginFill (text_color);
				bullet_sprite.graphics.drawCircle (int(bullet_indent / 2), item_text_sprite.height / 2, 2);
				element_sprite.addChild (bullet_sprite);}

			else if (bullet_type == "number"){
				var bullet_text:Sprite = GeneralText.MakeText (String(bullet_counter++ + 1) + ".", text_size, text_color);
				element_sprite.addChild (bullet_text);}

			else if (bullet_type == "letter"){
				bullet_text = GeneralText.MakeText (String.fromCharCode("a".charCodeAt(0) + bullet_counter++) + ")", text_size, text_color);
				element_sprite.addChild (bullet_text);}

			else if (bullet_type == "bullet"){
				var bullet_url:String = Course.ResolveMediaPath ("icons", "bullet.png");
				var bullet_loader:CustomLoader = GeneralLoader.LoadImage (bullet_url);
				element_sprite.addChild (bullet_loader);}

			element_width = element_sprite.width;
			element_height = element_sprite.height;
			return true;
		}
		public override function DocumentElement ():String
		{
			var text:String = xml.text;
			return "<p>&bull; " + text + "</p>";
		}
	}
}
