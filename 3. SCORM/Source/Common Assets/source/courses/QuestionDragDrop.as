package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.general.*;

	public class QuestionDragDrop extends Question
	{
		public var options_drag:Array = [];
		public var options_drop:Array = [];
		public var drop_width:int;
		public var drag_xpos:int;
		public var drag_ypos:int;
		public var drop_xpos:int;
		public var drop_ypos:int;

		public function QuestionDragDrop (xml:XML):void
		{
			super (xml);
		}
		public override function RenderQuestion ():Sprite
		{
			question_sprite = new Sprite;
			var head_sprite:Sprite = GetQuestionHead();
			question_sprite.addChild (head_sprite);

			drop_xpos = 0;
			drop_ypos = head_sprite.height + text_gap;
			if (options_xpos != 0) drag_xpos = drop_xpos = options_xpos;
			if (options_ypos != 0) drop_ypos = options_ypos;

			LayoutDropWindows (question_sprite);
			LayoutDragBoxes (question_sprite);

			return question_sprite;
		}
		public function LayoutDropWindows (question_sprite:Sprite=null):void
		{
			var xpos:int = drop_xpos;
			var max_height:int = 0;
			for each (var droption:OptionDropWindow in options_drop){
				droption.x = xpos;
				droption.y = drop_ypos;
				droption.Render ();
				xpos += drop_width + Option.box_gap;
				if (question_sprite){
					question_sprite.addChild (droption);}
				max_height = Math.max (max_height, droption.height);}
			drag_ypos = drop_ypos + max_height + Option.box_gap * 2;
		}
		public function LayoutDragBoxes (question_sprite:Sprite=null):void
		{
			var xpos:int = drag_xpos;
			var ypos:int = drag_ypos;
			var area_width:int = options_width ? options_width : Course.page_width;

			for each (var dragtion:OptionDrag in options_drag){
				if (!dragtion.drop_window){
					if (!dragtion.dragging){
						if (xpos + dragtion.box_width > drag_xpos + area_width){
							xpos = drag_xpos;
							ypos += dragtion.box_height + Option.box_gap;}
						dragtion.ShowAt (xpos, ypos);
						xpos += dragtion.box_width + Option.box_gap;}}}

			if (question_sprite){
				for each (dragtion in options_drag){
					question_sprite.addChild (dragtion);}}
		}
		public function DownHandler (option:Option):void
		{
			question_sprite.addChild (option);
		}
		public function DragHandler (option:Option):void
		{
			var dragtion:OptionDrag = OptionDrag (option);
			var old_window:OptionDropWindow = dragtion.drop_window;

			var found_window:Boolean = false;
			for each (var droption:OptionDropWindow in options_drop){
				if (DragTest (dragtion, droption)){
					if (old_window){
						if (old_window == droption){
							return;}
						old_window.RemoveOption (dragtion);}						
					dragtion.drop_window = droption;
					droption.drop_array.push (dragtion);
					found_window = true;
					break;}}

			if (!found_window && old_window){
				old_window.RemoveOption (dragtion);
				dragtion.drop_window = null;
				dragtion.desired_scale = 1;}

			LayoutDropWindows ();
			LayoutDragBoxes ();
		}
		public function DragTest (dragtion:Option, droption:Option):Boolean
		{
			var fudge_x:int = dragtion.width / 2.5;
			var fudge_y:int = dragtion.height / 2.5;
			return (dragtion.x >= droption.x - fudge_x &&
				dragtion.y >= droption.y - fudge_y &&
				dragtion.x + dragtion.width <= droption.x + droption.width + fudge_x &&
				dragtion.y + dragtion.height <= droption.y + droption.height + fudge_y);
		}
		public function DropHandler (option:Option):void
		{
			DragHandler (option);
			Activity (option);
		}
		public override function OnEnterFrame (event:Event):void
		{
			for (var i:int = 0; i < options_drag.length; i++){
				var option:OptionDrag = options_drag[i];
				option.OnEnterFrame (event);}
		}
		public override function Cheat():void
		{
			for each (var droption:OptionDropWindow in options_drop){
				droption.drop_array = [];}
			for each (var dragtion:OptionDrag in options_drag){
				for each (droption in options_drop){
					if (droption.option_group == dragtion.option_group){
						dragtion.drop_window = droption;
						droption.drop_array.push (dragtion);
						break;}}}
			LayoutDropWindows ();
			LayoutDragBoxes ();
			super.Cheat();
		}
		public override function IsCorrect ():Boolean
		{
			for each (var dragtion:OptionDrag in options_drag){
				if (!dragtion.drop_window || dragtion.drop_window.option_group != dragtion.option_group){
					return false;}}
			return true;
		}
		public override function StudentResponse():String
		{
			var response:String = "";
			for each (var droption:OptionDropWindow in options_drop){
				if (response.length) response += ",";
				response += droption.option_group;
				for each (var dragtion:OptionDrag in options_drag){
					if (dragtion.drop_window == droption){
						response += "." + dragtion.option_ord;}}}
			return response;
		}
		public override function CorrectResponse():String
		{
			var response:String = "";
			for each (var droption:OptionDropWindow in options_drop){
				if (response.length) response += ",";
				response += droption.option_group;
				for each (var dragtion:OptionDrag in options_drag){
					if (droption.option_group == dragtion.option_group){
						response += "." + dragtion.option_ord;}}}
			return response;
		}
		public override function Reset():void
		{
			for each (var dragtion:OptionDrag in options_drag){
				dragtion.drop_window = null;}
			for each (var droption:OptionDropWindow in options_drop){
				droption.drop_array = [];}
		}
	}
}
