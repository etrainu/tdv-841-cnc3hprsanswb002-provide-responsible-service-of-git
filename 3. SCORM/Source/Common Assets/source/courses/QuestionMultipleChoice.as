﻿package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import source.general.*;

	public class QuestionMultipleChoice extends Question
	{
		private var question_options:Array = [];
		private var answer_count:int;

		public function QuestionMultipleChoice (ques_xml:XML):void
		{
			super (ques_xml);

			var option_xml_list:XMLList = ques_xml.option;
			for each (var option_xml:XML in option_xml_list){
				var option:OptionMultipleChoice = new OptionMultipleChoice (option_xml);
				if (option.IsCorrectAnswer()) answer_count++;
				question_options.push (option);}
		}
		public override function IsCorrect ():Boolean
		{
			for each (var option:OptionMultipleChoice in question_options){
				if (!option.IsCorrect())
					return false;}
			return true;
		}
		public override function RenderQuestion ():Sprite
		{
			var question_sprite:Sprite = new Sprite;
			var head_sprite:Sprite = GetQuestionHead();
			question_sprite.addChild (head_sprite);

			var xpos:int = 0;
			var ypos:int = head_sprite.height + text_gap;
			if (options_xpos != 0) xpos = options_xpos;
			if (options_ypos != 0) ypos = options_ypos;

			for (var i:int = 0; i < question_options.length; i++){
				var option:OptionMultipleChoice = question_options[i];
				option.x = xpos;
				option.y = ypos;
				ypos += option.height + Option.text_gap;
				question_sprite.addChild (option);

				option.addEventListener (MouseEvent.CLICK, OptionClicked);}
			return question_sprite;
		}
		public function OptionClicked (event:MouseEvent):void
		{
			var option:OptionMultipleChoice = OptionMultipleChoice (event.currentTarget);

			if (answer_count == 1){
				UncheckAll ();
				option.Check();
				if (activity_handler != null){
					activity_handler (this, option);}}
			else {
				option.Toggle();
				if (CountResponses() >= answer_count){
					if (activity_handler != null){
						activity_handler (this, option);}}}
		}
		public override function GetQuestionHead ():Sprite
		{
			var sprite:Sprite = super.GetQuestionHead ();

			if (answer_count > 1){
				//var count_text:String = "( Tick " + answer_count + " answers )";
				var count_text:String = "";
				var count_sprite:Sprite = GeneralText.MakeText (count_text, help_size, help_color, 0, sprite.height+8);
				sprite.addChild (count_sprite);}

			return sprite;
		}
		public function CountResponses ():int
		{
			var count:int = 0;
			for (var i:int=0; i<question_options.length; i++){
				var option:OptionMultipleChoice = question_options[i];
				if (option.IsChecked()) count++;}
			return count;
		}
		public function UncheckAll ():void
		{
			for each (var option:OptionMultipleChoice in question_options){
				option.Uncheck();}
		}
		public override function Reset():void
		{
			UncheckAll ();
		}
		public override function Cheat():void
		{
			for each (var option:OptionMultipleChoice in question_options){
				option.Cheat();}
			super.Cheat();
		}
		public override function StudentResponse():String
		{
			var response:String = "";
			for (var i:int=0; i<question_options.length; i++){
				var option:OptionMultipleChoice = question_options[i];
				if (response.length) response += ",";
				response += option.IsChecked() ? "1":"0";}
			return response;
		}
		public override function CorrectResponse():String
		{
			var response:String = "";
			for (var i:int=0; i<question_options.length; i++){
				var option:OptionMultipleChoice = question_options[i];
				if (response.length) response += ",";
				response += option.IsCorrectAnswer() ? "1":"0";}
			return response;
		}
	}
}

